<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus"></i> Add Products</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('peremajaan/saveData', ['class' => 'form-add']); ?>
            <?= csrf_field(); ?>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Lokasi</label>
                    <div class="col-sm-10">
                        <select name="id_lokasi" id="id_lokasi">
                        <?php foreach($dataLokasi as $lokasi) : ?>
                            <option value="<?= $lokasi->lokasi_id;?>"><?= esc($lokasi->lokasi);?></option>
                        <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback errorId_lokasi">

                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Tanggal Req</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="tgl_req" name="tgl_req">
                        <div class="invalid-feedback errorTgl_req">

                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-sm-2 col-form-label">Peremajaan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="jenis_peremajaan" name="jenis_peremajaan">
                        <div class="invalid-feedback errorJenis_peremajaan">

                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select name="status" id="status">
                        <?php
                            $statusPeremajaans=unserialize(STATUS_PEREMAJAAN);           
                            foreach ($statusPeremajaans as $status => $value) {
                            echo '<option value="'.$statusPeremajaans[$status].'">'.$statusPeremajaans[$status].'</option>';
                            }
                        ?>
                        </select>
                        <div class="invalid-feedback errorDate">

                        </div>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="price" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="note" name="note">
                        <div class="invalid-feedback errorNote">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary btn-sm btn-save">Save</button>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.form-add').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.btn-save').attr('disable', 'disabled');
                    $('.btn-save').html('<i class="fas fa-spin fa-spinner"></i>');
                },
                complete: function() {
                    $('.btn-save').removeAttr('disable');
                    $('.btn-save').html('Save');
                },
                success: function(response) {
                    if (response.error) {
                        if (response.error.name) {
                            $('#name').addClass('is-invalid');
                            $('.errorName').html(response.error.name);
                        } else {
                            $('#name').removeClass('is-invalid');
                            $('.errorName').html('');
                        }

                        if (response.error.description) {
                            $('#description').addClass('is-invalid');
                            $('.errorDesc').html(response.error.description);
                        } else {
                            $('#description').removeClass('is-invalid');
                            $('.errorDesc').html('');
                        }

                        if (response.error.price) {
                            $('#price').addClass('is-invalid');
                            $('.errorPrice').html(response.error.price);
                        } else {
                            $('#price').removeClass('is-invalid');
                            $('.errorPrice').html('');
                        }

                        if (response.error.date) {
                            $('#date').addClass('is-invalid');
                            $('.errorDate').html(response.error.date);
                        } else {
                            $('#date').removeClass('is-invalid');
                            $('.errorDate').html('');
                        }

                    } else {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: response.success,
                            showConfirmButton: false,
                            timer: 1800
                        })

                        $('#addModal').modal('hide');
                        getPeremajaans();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
            return false;
        });
    });
</script>