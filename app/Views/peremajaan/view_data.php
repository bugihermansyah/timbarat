 <div class="table-responsive">
     <table class="table table-bordered table-striped table-sm" id="getPeremajaans" width="100%" cellspacing="0">
         <thead>
             <tr>
                 <th>No</th>
                 <th>Lokasi</th>
                 <th>Tanggal</th>
                 <th>Peremajaan</th>
                 <th>Status</th>
                 <th>Keterangan</th>
                 <th></th>
             </tr>
         </thead>
         <tbody>
            <?php $no = 1;
                foreach ($dataPeremajaan as $datas) : ?>
                 <tr>
                    <td width="1%"><?= $no++; ?></td>
                    <td><?= esc($datas['lokasi']); ?></td>
                    <td><?= esc($datas['tgl_req']); ?></td>
                    <td><?= esc($datas['jenis_peremajaan']); ?></td>
                    <td>
                        <span class="badge badge-<?= esc($datas['status']) == 'Finish' ? 'success':(esc($datas['status']) == 'Progress' ? 'warning' : 'danger') ; ?>"><?= esc($datas['status']); ?></td>
                        </span>    
                    <td><?= esc($datas['note']); ?></td>
                    <td class="text-center" width="10%">
                        <a href="#" class="btn btn-sm btn-outline-primary" onclick="edit('<?= $datas['peremajaan_id']; ?>')">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-outline-danger" onclick="deletes('<?= $datas['peremajaan_id']; ?>')">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                 </tr>
            <?php endforeach; ?>
         </tbody>
     </table>
 </div>

 <script>

     $(document).ready(function() {
         $('#getPeremajaans').DataTable();
     });

     function edit(peremajaan_id) {
         $.ajax({
             type: "post",
             url: "<?= base_url('peremajaan/getModalEdit'); ?>",
             data: {
                 peremajaan_id: peremajaan_id
             },
             dataType: "json",
             success: function(response) {
                 if (response.output) {
                     $('.view-modal').html(response.output).show();
                     $('#editModal').modal('show');
                 }
             },
             error: function(xhr, ajaxOptions, thrownError) {
                 alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
             }
         });
     }

     function deletes(peremajaan_id) {
         Swal.fire({
             title: 'Are you sure?',
             text: `You won't be able to revert this!`,
             icon: 'question',
             showCancelButton: true,
             confirmButtonColor: '#d33',
             cancelButtonColor: '#3085d6',
             confirmButtonText: 'Yes, delete it!',
             cancelButtonText: 'Cancel'
         }).then((result) => {
             if (result.value) {
                 $.ajax({
                     type: "post",
                     url: "<?= base_url('peremajaan/deleteData'); ?>",
                     data: {
                         peremajaan_id: peremajaan_id
                     },
                     dataType: "json",
                     success: function(response) {
                         if (response.output) {
                             Swal.fire({
                                 icon: 'success',
                                 title: 'Deleted',
                                 text: response.output,
                             });
                             getPeremajaans();
                         }
                     },
                     error: function(xhr, ajaxOptions, thrownError) {
                         alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                     }
                 });
             }
         });
     }
     
 </script>