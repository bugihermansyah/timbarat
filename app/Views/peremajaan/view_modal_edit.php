<!-- Modal -->
<div class="modal fade" data-backdrop="static" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-edit"></i> Edit Peremajaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('peremajaan/updateData', ['class' => 'form-edit']); ?>
            <?= csrf_field(); ?>
            <input type="hidden" name="peremajaan_id" value="<?= $peremajaan_id; ?>">
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Lokasi</label>
                    <div class="col-sm-10">
                        <select name="id_lokasi" id="id_lokasi">
                        <?php foreach($dataLokasi as $lokasi) : ?>
                            <?php if ($lok = $id_lokasi ?? $id_lokasi) : ?>
                            <option value="<?= $lokasi->lokasi_id ;?>" <?= $lok == $lokasi->lokasi_id ? 'selected' : null ?>>
                                <?= $lokasi->lokasi ;?>
                            </option>
                            <?php endif ;?>
                        <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback errorId_lokasi">

                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tgl_req" class="col-sm-2 col-form-label">Tanggal Request</label>
                    <div class="col-sm-10">
                        <input type="date" name="tgl_req" id="tgl_req" class="form-control" value="<?= $tgl_req; ?>">
                        <div class="invalid-feedback errorTgl_req">

                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tgl_req" class="col-sm-2 col-form-label">Jenis Peremajaan</label>
                    <div class="col-sm-10">
                        <textarea name="jenis_peremajaan" id="jenis_peremajaan" class="form-control"><?= $jenis_peremajaan; ?></textarea>
                        <div class="invalid-feedback errorJenis_peremajaan">

                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tgl_req" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select name="status" id="status">
                            <?php $statusPeremajaans=unserialize(STATUS_PEREMAJAAN); 
                            foreach ($statusPeremajaans as $statuss => $value) : 
                                if($stat = $status ?? $status) :?>
                                <option value="<?= $statusPeremajaans[$statuss] ?>" <?= 
                                $stat == $statusPeremajaans[$statuss] ? 'selected': null ?>><?= $statusPeremajaans[$statuss] ?></option>
                                <?php endif ;?>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback errorStatus">

                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tgl_req" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea name="note" id="note" rows="4" class="form-control"><?= $note; ?></textarea>
                        <div class="invalid-feedback errorNote">

                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary btn-sm btn-update">Update</button>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.form-edit').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.btn-update').attr('disable', 'disabled');
                    $('.btn-update').html('<i class="fas fa-spin fa-spinner"></i>');
                },
                complete: function() {
                    $('.btn-update').removeAttr('disable');
                    $('.btn-update').html('Update');
                },
                success: function(response) {
                    if (response.error) {
                        if (response.error.id_lokasi) {
                            $('#id_lokasi').addClass('is-invalid');
                            $('.errorId_lokasi').html(response.error.id_lokasi);
                        } else {
                            $('#id_lokasi').removeClass('is-invalid');
                            $('.errorId_lokasi').html('');
                        }

                        if (response.error.tgl_req) {
                            $('#tgl_req').addClass('is-invalid');
                            $('.errorTgl_req').html(response.error.tgl_req);
                        } else {
                            $('#tgl_req').removeClass('is-invalid');
                            $('.errorTgl_req').html('');
                        }

                        if (response.error.jenis_peremajaan) {
                            $('#jenis_peremajaan').addClass('is-invalid');
                            $('.errorJenis_peremajaan').html(response.error.jenis_peremajaan);
                        } else {
                            $('#jenis_peremajaan').removeClass('is-invalid');
                            $('.errorJenis_peremajaan').html('');
                        }

                        if (response.error.status) {
                            $('#status').addClass('is-invalid');
                            $('.errorStatus').html(response.error.status);
                        } else {
                            $('#status').removeClass('is-invalid');
                            $('.errorStatus').html('');
                        }

                        if (response.error.note) {
                            $('#note').addClass('is-invalid');
                            $('.errorNote').html(response.error.note);
                        } else {
                            $('#note').removeClass('is-invalid');
                            $('.errorNote').html('');
                        }

                    } else {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: response.success,
                            showConfirmButton: false,
                            timer: 1800
                        })

                        $('#editModal').modal('hide');
                        getPeremajaans();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
            return false;
        });
    });
</script>