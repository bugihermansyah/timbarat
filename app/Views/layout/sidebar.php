<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url();?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Tim Barat <sup>21</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item <?= current_url() == base_url('dashboard') ? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url('dashboard'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Master
    </div>

    <li class="nav-item <?= current_url() == base_url('perusahaan') ? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url('perusahaan')?>">
            <i class="fas fa-fw fa-building"></i>
            <span>Perusahaan</span></a>
    </li>

    <li class="nav-item <?= current_url() == base_url('lokasi') ? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url('lokasi')?>">
            <i class="fas fa-fw fa-map-marker-alt"></i>
            <span>Lokasi</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOthers"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-ethernet"></i>
            <span>Others</span>
        </a>
        <div id="collapseOthers" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="report/outstanding">Tags</a>
                <a class="collapse-item" href="#">Produk</a>
                <a class="collapse-item" href="">Support</a>
                <div class="collapse-divider"></div>
            </div>
        </div>
    </li>

   <!-- Divider -->
   <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Reminders
    </div>

    <!-- Nav Item - Charts -->
    <li class="nav-item <?= current_url() == base_url('laporan') ? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url('laporan')?>">
            <i class="fas fa-fw fa-bug"></i>
            <span>Laporan</span></a>
    </li>

    <li class="nav-item <?= current_url() == base_url('peremajaan') ? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url('peremajaan')?>">
            <i class="fas fa-fw fa-upload"></i>
            <span>Peremajaan</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item <?= current_url() == base_url('request') ? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url('request')?>">
            <i class="fas fa-fw fa-hand-holding-usd"></i>
            <span>Request</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    
    <!-- Heading -->
    <div class="sidebar-heading">
        Reports
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-paste"></i>
            <span>Exports</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <!-- <h6 class="collapse-header">Login Screens:</h6> -->
                <a class="collapse-item" href="/report/outstanding">Outstanding</a>
                <a class="collapse-item" href="/report/kerusakan">Tags</a>
                <a class="collapse-item" href="/report/lokasiblok">Lokasi Blok</a>
                <a class="collapse-item" href="">SLA</a>
                <div class="collapse-divider"></div>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->