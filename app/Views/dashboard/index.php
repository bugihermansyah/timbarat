<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
        </div>

        <!-- Content Row -->
        <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Open All Cases</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_open_all ;?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-bug fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4 d-none d-sm-block">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    All Lokasi</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_lokasi_all ;?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-location fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4 d-none d-sm-block">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Open Request
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                                    </div>
                                    <div class="col">
                                        <div class="progress progress-sm mr-2">
                                            <div class="progress-bar bg-info" role="progressbar"
                                                style="width: 50%" aria-valuenow="50" aria-valuemin="0"
                                                aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Open All Requests</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_req_all ;?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-truck fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content Req unit -->
        <div class="row">
            <div class="col-xl-8" style="margin-bottom:10px">
                <div class="card">
                    <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                        <h4 class="mb-0">Reminder request unit</h3>
                        </div>
                        <div class="col text-right">
                        <a href="<?= base_url('request');?>" class="btn btn-sm btn-primary">See all</a>
                        </div>
                    </div>
                    </div>
                    <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-sm align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                        <th>Lokasi</th>
                                <th>Item</th>
                                <th>Pengiriman</th>
                                <th>Jatuh Tempo</th>
                                <th>Selisih</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($all_jatuhtempo as $datas) : ?>
                            <tr style="font-size:14px">
                                <td><?= $datas->lokasi ;?></td>
                                <td><?= esc($datas->item) ;?></td>
                                <td><?= $datas->tglpengiriman == null ? "no-update" : date("d M Y", strtotime($datas->tglpengiriman)) ?></td>
                                <td><?= $datas->tglpengiriman == null ? "no-update" : date("d M Y", strtotime($datas->jatuh_tempo)) ?></td>
                                <td><span class="badge badge-<?= $datas->selisih < 2 ? "danger" : "success" ?> badge-pill">
                                <?php
                                if ($datas->tglpengiriman == null) {
                                    echo "no-update";
                                }?>
                                    <?= esc($datas->selisih) > 0 ? $datas->selisih." hari lagi" : (esc($datas->selisih) == 0 ? "Hari ini" : (esc($datas->selisih)." hari lewat"))?>
                                </span></td>
                            </tr>
                        <?php endforeach ;?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-4" style="margin-bottom:10px">
                <div class="card">
                    <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                        <h5 class="mb-0">Sisa Pendingan</h3>
                        </div>
                    </div>
                    </div>
                    <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-sm align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">PIC</th>
                            <th scope="col"  class="text-center">Case /masalah</th>
                            <th scope="col"  class="text-center">Request /unit</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr  style="font-size:14px">
                                <td>Bugi</td>
                                <td class="text-center"><?= $count_open_bugi ;?></td>
                                <td class="text-center"><?= $count_req_bugi ;?></td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>Roma</td>
                                <td class="text-center"><?= $count_open_roma ;?></td>
                                <td class="text-center"><?= $count_req_roma ;?></td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>Fajar</td>
                                <td class="text-center"><?= $count_open_fajar ;?></td>
                                <td class="text-center"><?= $count_req_fajar ;?></td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>Husein</td>
                                <td class="text-center"><?= $count_open_husein ;?></td>
                                <td class="text-center"><?= $count_req_husein ;?></td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>Hasbu</td>
                                <td class="text-center"><?= $count_open_hasbu ;?></td>
                                <td class="text-center"><?= $count_req_hasbu ;?></td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>Dhaysal</td>
                                <td class="text-center"><?= $count_open_dhaysal ;?></td>
                                <td class="text-center"><?= $count_req_dhaysal ;?></td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>Dana</td>
                                <td class="text-center"><?= $count_open_dana ;?></td>
                                <td class="text-center"><?= $count_req_dana ;?></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content Case -->
        <div class="row">
            <div class="col-xl-8" style="margin-bottom:10px">
                <div class="card">
                    <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                        <h4 class="mb-0">Cases pending</h3>
                        </div>
                        <div class="col text-right">
                        <a href="<?= base_url('laporan');?>" class="btn btn-sm btn-primary">See all</a>
                        </div>
                    </div>
                    </div>
                    <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-sm align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th>Lokasi</th>
                            <th>Masalah</th>
                            <th>PIC</th>
                            <th>Lapor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($select_laporan as $datas) : ?>
                            <tr style="font-size:14px">
                                <td><?= esc($datas->lokasi) ;?></td>
                                <td><?= esc($datas->masalah) ;?></td>
                                <td><?= esc($datas->pic) ;?></td>
                                <td><?= date("d M Y", strtotime($datas->tgl_add)) ;?></td>
                            </tr>
                        <?php endforeach ;?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-4" style="margin-bottom:10px">
                <div class="card">
                    <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                        <h5 class="mb-0">log activity</h3>
                        </div>
                    </div>
                    </div>
                    <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-sm align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">pic</th>
                            <th scope="col"  class="text-center">action</th>
                            <th scope="col"  class="text-center">datetime</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr  style="font-size:14px">
                                <td>bh</td>
                                <td class="text-center">add laporan id:27</td>
                                <td class="text-center">12-07 12:02:21</td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>bh</td>
                                <td class="text-center">add laporan id:27</td>
                                <td class="text-center">12-07 12:02:21</td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>bh</td>
                                <td class="text-center">add laporan id:27</td>
                                <td class="text-center">12-07 12:02:21</td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>bh</td>
                                <td class="text-center">add laporan id:27</td>
                                <td class="text-center">12-07 12:02:21</td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>bh</td>
                                <td class="text-center">add laporan id:27</td>
                                <td class="text-center">12-07 12:02:21</td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>bh</td>
                                <td class="text-center">add laporan id:27</td>
                                <td class="text-center">12-07 12:02:21</td>
                            </tr>
                            <tr  style="font-size:14px">
                                <td>bh</td>
                                <td class="text-center">add laporan id:27</td>
                                <td class="text-center">12-07 12:02:21</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>

<?= $this->endSection(); ?>