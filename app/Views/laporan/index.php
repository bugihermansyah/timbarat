<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>
            
        <script>
        $(document).ready(function() {
            $('#dataTable').DataTable( {
                "order": [ 1, 'asc' ]
            } );
        });
        </script>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Laporan</h6>
            </div>
            <div class="card-body">
            <a href="" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#addModal"><i class="fas fa-plus-square"></i> Add data</a>

            <?php
            $errors = session()->getFlashdata('failed');
            if (!empty($errors)) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-times"></i> Failed</strong> data not added to database.
                    <ul>
                        <?php foreach ($errors as $e) { ?>
                            <li><?= esc($e); ?></li>
                        <?php } ?>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <?php if (session()->getFlashData('success')) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-check"></i> Success</strong> <?= session()->getFlashData('success'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-sm nowrap" id="dataTableL" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Lokasi</th>
                                <th>Masalah</th>
                                <th>Progress</th>
                                <th>Stat</th>
                                <th>PIC</th>
                                <th>Pelapor</th>
                                <th>Lapor</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Lokasi</th>
                                <th>Masalah</th>
                                <th>Progress</th>
                                <th>Stat</th>
                                <th>PIC</th>
                                <th>Pelapor</th>
                                <th>Lapor</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </tfoot>
                        <tbody style="font-size:14px">
                            <?php $no = 1;
                            foreach ($all_data as $datas) : ?>
                                <tr>
                                    <td width="1%"><?= $no++ ;?></td>
                                    <td><?= esc($datas->lokasi); ?></td>
                                    <td><?= esc($datas->masalah); ?></td>
                                    <td>
                                        <?php foreach ($all_laporandetail as $laporandetail) : ?>
                                            <?php if ($laporandetail->id_laporan == $datas->id) : ?>
                                                <a <?= esc($laporandetail->link) == NULL ? '' : 'target="_blank"'; ?> class="badge badge-<?= $laporandetail->status == "Pending" ? "warning" : "success";?>" data-toggle="tooltip" data-placement="top" title="<?= date("d M yy", strtotime($laporandetail->tglvisit)); ?>">
                                                    <?= esc($laporandetail->status) == "Pending" ? "Pend" : "Finsh" ?>
                                                </a>
                                            <?php endif; ?>
                                        <?php endforeach; ?><br>
                                    </td>
                                    <td><input id="status" data-id="<?= $datas->id ?>" class="toggle-class" type="checkbox" data-toggle="toggle" data-offstyle="dark" data-on="Op" data-off="Cl" data-size="xs" <?= $datas->open == "Open" ? "checked":"" ?>></td>
                                    <td><?= esc($datas->pic); ?></td>
                                    <td><?= esc($datas->pelapor); ?></td>
                                    <td style="padding-left: 5px"><?= date("d M y", strtotime($datas->tgl_add)); ?></td>
                                    <td width="11%" class="text-center" style="padding-left: 1px;padding-right: 1px;">
                                        <div class="dropdown dropleft" style="display:inline">
                                            <a class="btn btn-outline-danger btn-sm dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></a>
                                            <!--Menu-->
                                            <div class="dropdown-menu dropdown-primary">
                                            <a href="laporan/edit/<?=$datas->id;?>" class="dropdown-item">
                                                <i class="fas fa-trash"></i> Tags
                                            </a>
                                            <a href="#" data-href="<?= base_url('laporan/'.$datas->id.'/delete') ?>" onclick="confirmToDelete(this)" class="dropdown-item btn-outline-danger"><i class="fas fa-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                        <a href="/laporan/aksi/<?=$datas->id;?>" class="btn btn-sm btn-outline-primary <?= ($datas->open == "Close") ? 'disabled' : '' ;?>" >
                                            <i class="fas fa-tools"></i>
                                        </a>
                                        <a href="/laporan/timeline/<?= $datas->id; ?>" class="btn btn-sm btn-outline-info">
                                            <i class="fas fa-search-plus"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    <!-- Add modal -->
    <div class="modal fade" id="addModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus-square"></i> Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= form_open('laporan/add_data'); ?>
                    <?= csrf_field(); ?>
                    <div class="form-group">
                        <label for="cabang" class="col-form-label col-form-label-sm">Lokasi</label>
                        <select name="id_lokasi" id="select2" class="form-control form-control-sm" style="width:100%" required autofocus>
                            <option value="">- Select -</option>
                            <?php foreach($all_lokasi as $lokasi) : ?>
                            <option value="<?= $lokasi->lokasi_id;?>"><?= esc($lokasi->lokasi);?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="Tanggal Lapor" class="col-form-label col-form-label-sm">Tanggal Lapor</label>
                        <input type="date" name="tgl_add" id="Tanggal Lapor" class="form-control form-control-sm form-control form-control-sm-sm" required>
                    </div>
                    <div class="form-group">
                        <label for="pelapor" class="col-form-label col-form-label-sm">Pelapor</label>
                        <select name="pelapor" id="Pelapor" class="form-control form-control-sm form-control form-control-sm-sm" required>
                            <option value="">- Select -</option>
                            <option value="Preventif">Preventif</option>
                            <option value="Client">Client</option>
                            <option value="Support">Support</option>
                        </select>
                    </div>
                    
                    <label for="Masalah" class="col-form-label col-form-label-sm">Masalah</label>
                    <div class="input-group inputitem fieldGroup">
                        <input type="text" class="form-control form-control-sm" name="masalah[]" placeholder="Masalah" aria-label="Masalah baru" aria-describedby="button-addon2" required>
                        <div class="input-group-append">
                            <button class="btn btn-outline-primary addMore" type="button" id="button-addon2"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>    

    <div class="input-group fieldGroupCopy" style="display: none;">
        <input type="text" class="form-control form-control-sm" name="masalah[]" placeholder="Masalah" aria-label="Recipient's username" aria-describedby="button-addon2" required>
        <div class="input-group-append">
            <button class="btn btn-outline-danger remove" type="button" id="button-addon2"><i class="fas fa-trash"></i></button>
        </div>
    </div>

    <div id="confirm-dialog" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-body">
                <h2 class="h2">Are you sure?</h2>
                <p>The data will be deleted and lost forever</p>
            </div>
            <div class="modal-footer">
                <a href="#" role="button" id="delete-button" class="btn btn-danger">Delete</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
            </div>
        </div>
    </div>

    <script>
        function confirmToDelete(el){
            $("#delete-button").attr("href", el.dataset.href);
            $("#confirm-dialog").modal('show');
        }
    </script>
    <script>
        $(document).ready(function(){
            $("#dataTableL").DataTable({
                "destroy"       : true,
                "paging"        : true,
                pageLength      : 25,
                "drawCallback"  : function(settings) {
                    // alert( 'DataTables has redrawn the table' );
                    $('.toggle-class').bootstrapToggle();

                    $('.toggle-class').on('change', function(){
                        var open = $(this).prop('checked') == true ? 'Open' : 'Close';
                        var laporan_id = $(this).data('id');
                            $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: '/laporan/update_open/'+laporan_id,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            data: {'open':open, 'laporan_id':laporan_id},
                                success: function(data){
                                    consol.log('Success')
                                }
                            });
                    });
                }
            });
        });
    </script>
<?= $this->endSection(); ?>