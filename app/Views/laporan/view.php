<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Timeline Outstanding</h6>
        </div>
        <div class="card-body">
        <div class="card" style="margin-bottom:15px">
            <div class="card-body">
                <div class="row text-left text-xs-left text-sm-left text-md-left">
                    <div class="col-xs-12 col-sm-4 col-md-6">
                        <!-- <h5>Quick links</h5> -->
                        <hr>
                        <ul class="list-unstyled quick-links">
                            <li><i class="fa fa-angle-double-right"></i>Lokasi : <?= $laporanlokasi->lokasi ?></li>
                            <li><i class="fa fa-angle-double-right"></i>Masalah : <?= $laporanlokasi->masalah ?></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <!-- <h5>Quick links</h5> -->
                        <hr>
                        <ul class="list-unstyled quick-links">
                            <li><i class="fa fa-angle-double-right "></i>Onsite <span class="badge badge-info"><?= $onsite ?></span></li>
                            <li><i class="fa fa-angle-double-right"></i>Remote <span class="badge badge-info"><?= $remote ?></span></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <!-- <h5>Quick links</h5> -->
                        <hr>
                        <ul class="list-unstyled quick-links">
                            <li><i class="fa fa-angle-double-right"></i>Last Status : <span class="badge badge-<?= $getLastData->status == "Finish" ? "success":"warning" ?>"><?= $getLastData->status ?></span></li>
                            <li><i class="fa fa-angle-double-right"></i>Tags : 
                            <?php foreach($getTags as $tag) : ?>
                                <span class="badge badge-info"><?= $tag->name ?></span>
                            <?php endforeach ?>
                            <?php if($getTags == NULL) :?>
                                <?= "Data tidak ada." ?>
                            <?php endif ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <hr>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Unit</th>
                                <th scope="col">Tanggal Req</th>
                                <th scope="col">Tanggal Kirim</th>
                                <th scope="col">Tanggal Ambil</th>
                                <th scope="col">Terkirim</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($getrequesttimeline as $req) : ?>
                                <tr>
                                    <th width="1%"><?= $no++ ;?></th>
                                    <td><?= $req->item ?></td>
                                    <td><?= $req->tglrequest == null ? "" : date("d M Y", strtotime($req->tglrequest)) ?></td>
                                    <td><?= $req->tglpengiriman == null ? "" : date("d M Y", strtotime($req->tglpengiriman)) ?></td>
                                    <td><?= $req->tglpengambilan == null ? "" : date("d M Y", strtotime($req->tglpengambilan)) ?></td>
                                    <td><?= $req->status ?></td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Content -->
        <div class="col-md-12">
            <ul class="cbp_tmtimeline">
                <?php
                foreach ($laporandetail as $lap) : ?>
                <li>
                    <time class="cbp_tmtime" ><span><?= date("d M Y", strtotime($lap->tglvisit)) ?></span> <span><?= $lap->status ?></span></time>
                    <div class="cbp_tmicon <?= $lap->status == 'Pending' ? 'bg-info' : 'bg-green'; ?>"><i class="fas <?= $lap->status == 'Pending' ? 'fa-minus' : 'fa-check'; ?>"></i></div>
                    <div class="cbp_tmlabel">
                        <h2><i class="fas fa-user"> <?= $lap->pic ?></i> <span><?= $lap->onsite == '1' ? 'visited' : 'remoted'; ?></span></h2>
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="">Sebab :</label>
                                <hr style = "margin:0">
                                <?= $lap->sebab ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Aksi :</label>
                                <hr style = "margin:0">
                                <?= $lap->aksi ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Solusi :</label>
                                <hr style = "margin:0">
                                <?= $lap->solusi ?>
                            </div>
                        </div>
                    </div>
                </li>
                <?php endforeach ?>
            </ul>  
        </div>

<style type="text/css">
/* body{
    margin-top:20px;
    background:#FBFDFF
} */
.cbp_tmtimeline {
    margin: 0;
    padding: 0;
    list-style: none;
    position: relative
}

.cbp_tmtimeline:before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    width: 3px;
    background: #eee;
    left: 20%;
    margin-left: -6px
}

.cbp_tmtimeline>li {
    position: relative
}

.cbp_tmtimeline>li:first-child .cbp_tmtime span.large {
    color: #444;
    font-size: 17px !important;
    font-weight: 700
}

.cbp_tmtimeline>li:first-child .cbp_tmicon {
    background: #fff;
    color: #666
}

.cbp_tmtimeline>li:nth-child(odd) .cbp_tmtime span:last-child {
    color: #444;
    font-size: 13px
}

.cbp_tmtimeline>li:nth-child(odd) .cbp_tmlabel {
    background: #f0f1f3
}

.cbp_tmtimeline>li:nth-child(odd) .cbp_tmlabel:after {
    border-right-color: #f0f1f3
}

.cbp_tmtimeline>li .empty span {
    color: #777
}

.cbp_tmtimeline>li .cbp_tmtime {
    display: block;
    width: 23%;
    padding-right: 70px;
    position: absolute
}

.cbp_tmtimeline>li .cbp_tmtime span {
    display: block;
    text-align: right
}

.cbp_tmtimeline>li .cbp_tmtime span:first-child {
    font-size: 15px;
    color: #3d4c5a;
    font-weight: 700
}

.cbp_tmtimeline>li .cbp_tmtime span:last-child {
    font-size: 14px;
    color: #444
}

.cbp_tmtimeline>li .cbp_tmlabel {
    margin: 0 0 15px 25%;
    background: #f0f1f3;
    padding: 1.2em;
    position: relative;
    border-radius: 5px
}

.cbp_tmtimeline>li .cbp_tmlabel:after {
    right: 100%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    border-right-color: #f0f1f3;
    border-width: 10px;
    top: 10px
}

.cbp_tmtimeline>li .cbp_tmlabel blockquote {
    font-size: 16px
}

.cbp_tmtimeline>li .cbp_tmlabel .map-checkin {
    border: 5px solid rgba(235, 235, 235, 0.2);
    -moz-box-shadow: 0px 0px 0px 1px #ebebeb;
    -webkit-box-shadow: 0px 0px 0px 1px #ebebeb;
    box-shadow: 0px 0px 0px 1px #ebebeb;
    background: #fff !important
}

.cbp_tmtimeline>li .cbp_tmlabel h2 {
    margin: 0px;
    padding: 0 0 10px 0;
    line-height: 26px;
    font-size: 16px;
    font-weight: normal
}

.cbp_tmtimeline>li .cbp_tmlabel h2 a {
    font-size: 15px
}

.cbp_tmtimeline>li .cbp_tmlabel h2 a:hover {
    text-decoration: none
}

.cbp_tmtimeline>li .cbp_tmlabel h2 span {
    font-size: 15px
}

.cbp_tmtimeline>li .cbp_tmlabel p {
    color: #444
}

.cbp_tmtimeline>li .cbp_tmicon {
    width: 40px;
    height: 40px;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    font-size: 1.4em;
    line-height: 40px;
    -webkit-font-smoothing: antialiased;
    position: absolute;
    color: #fff;
    background: #46a4da;
    border-radius: 50%;
    box-shadow: 0 0 0 5px #f5f5f6;
    text-align: center;
    left: 20%;
    top: 0;
    margin: 0 0 0 -25px
}

@media screen and (max-width: 992px) and (min-width: 768px) {
    .cbp_tmtimeline>li .cbp_tmtime {
        padding-right: 60px
    }
}

@media screen and (max-width: 65.375em) {
    .cbp_tmtimeline>li .cbp_tmtime span:last-child {
        font-size: 12px
    }
}

@media screen and (max-width: 47.2em) {
    .cbp_tmtimeline:before {
        display: none
    }
    .cbp_tmtimeline>li .cbp_tmtime {
        width: 100%;
        position: relative;
        padding: 0 0 20px 0
    }
    .cbp_tmtimeline>li .cbp_tmtime span {
        text-align: left
    }
    .cbp_tmtimeline>li .cbp_tmlabel {
        margin: 0 0 30px 0;
        padding: 1em;
        font-weight: 400;
        font-size: 95%
    }
    .cbp_tmtimeline>li .cbp_tmlabel:after {
        right: auto;
        left: 20px;
        border-right-color: transparent;
        border-bottom-color: #f5f5f6;
        top: -20px
    }
    .cbp_tmtimeline>li .cbp_tmicon {
        position: relative;
        float: right;
        left: auto;
        margin: -64px 5px 0 0px
    }
    .cbp_tmtimeline>li:nth-child(odd) .cbp_tmlabel:after {
        border-right-color: transparent;
        border-bottom-color: #f5f5f6
    }
}

.bg-green {
    background-color: #50d38a !important;
    color: #fff;
}

.bg-blush {
    background-color: #ff758e !important;
    color: #fff;
}

.bg-orange {
    background-color: #ffc323 !important;
    color: #fff;
}

.bg-info {
    background-color: #2CA8FF !important;
}
</style>
        </div>
    </div>

<?= $this->endSection(); ?>