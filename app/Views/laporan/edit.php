<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Add Tags</h6>
        </div>
        <div class="card-body">
        <?php
        $errors = session()->getFlashdata('failed');
        if (!empty($errors)) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong><i class="fas fa-times"></i> Failed</strong> data not added to database.
                <ul>
                    <?php foreach ($errors as $e) { ?>
                        <li><?= esc($e); ?></li>
                    <?php } ?>
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>

        <?php if (session()->getFlashData('success')) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong><i class="fas fa-check"></i> Success</strong> <?= session()->getFlashData('success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>

            <!-- Content -->
            <div class="row">
                <div class="col-md-6">
                    <?= form_open('laporan/edit_laporan/'.$getLaporan->laporan_id); ?>
                        <input name="laporan_id" type="hidden" value="<?= $getLaporan->laporan_id ?>">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Masalah</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input placeholder" value="<?= $getLaporan->masalah ?>">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Tags</label>
                            <select class="form-control js-example-basic-multiple" name="tags[]" multiple="multiple">
                                <?php
                                $array_tag = [];
                                foreach($getTagsId as $tagid) :
                                    $array_tag[] = $tagid->tag_id;
                                endforeach
                                ?>

                                <?php
                                foreach($all_tags as $tag) : ?>
                                    <option value="<?= $tag->tag_id ?>" <?= in_array($tag->tag_id, $array_tag) ? "selected" : "" ?>>
                                        <?= $tag->name ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Update</button>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection(); ?>