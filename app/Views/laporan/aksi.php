<?= $this->extend('layout/templates'); ?>
<?= $this->Section('content'); ?>

<?= form_open('laporan/add_laporandetail'); ?>
<div class="row">
    <div class="col-xl-8" style="margin-bottom:10px">
        <div class="card">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                    <h4 class="mb-0">Detail laporan</h3>
                    </div>
                    <div class="col text-right">
                        <a href="/laporan" class="btn btn-secondary active" role="button" aria-pressed="true">Back</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                    <input type="hidden" name="id_lokasi" value="<?= $getLaporan->lokasi_id?>">
                    <input type="hidden" name="id_laporan" value="<?= $getLaporan->laporan_id?>">
                        <div class="row">
                            <div class="col-md-4">    
                                <!-- <div class="alert alert-dark" role="alert" style="padding: 2px 8px; border-bottom-right-radius: 0; border-top-right-radius: 0;"> -->
                                    <?= $getLaporan->lokasi ?>
                                <!-- </div> -->
                                <hr>
                            </div>
                            <div class="col-md-8">
                                <!-- <div class="alert alert-warning" role="alert" style="padding: 2px 8px; border-bottom-left-radius: 0; border-top-left-radius: 0;"> -->
                                    <?= $getLaporan->masalah ?>
                                <!-- </div> -->
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="col-form-label col-form-label-sm">Sebab</label>
                                    <textarea name="sebab" class="form-control form-control-sm" id="exampleFormcontrol form-control-smTextarea1" rows="3" required autofocus></textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-form-label col-form-label-sm">Aksi</label>
                                    <textarea name="aksi" class="form-control form-control-sm" id="exampleFormcontrol form-control-smTextarea1" rows="3"></textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-form-label col-form-label-sm">Solusi</label>
                                    <textarea name="solusi" class="form-control form-control-sm" id="exampleFormcontrol form-control-smTextarea1" rows="3" required></textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-form-label col-form-label-sm">Note</label>
                                    <textarea name="note" class="form-control form-control-sm" id="exampleFormcontrol form-control-smTextarea1" rows="2"></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="onsite" class="col-form-label col-form-label-sm">Onsite/Remote</label>
                                    <select name="onsite" class="form-control form-control-sm" required>
                                        <option value="1" selected>Onsite</option>
                                        <option value="0">Remote</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="datedetail" class="col-form-label col-form-label-sm">Tanggal</label>
                                    <input type="date" name="datedetail" class="form-control form-control-sm" required>
                                </div>
                                <div class="form-group">
                                    <label for="cabang" class="col-form-label col-form-label-sm">PIC</label>
                                    <select name="pic" class="form-control form-control-sm" required>
                                        <option value="">- Select -</option>
                                    <?php foreach ($pics as $pic) : ?>
                                        <option value="<?= $pic->name;?>"><?= $pic->name;?></option>
                                    <?php endforeach ;?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="cabang" class="col-form-label col-form-label-sm">Status</label>
                                    <select name="status" id="status" class="form-control form-control-sm" required>
                                        <option value="">- Select -</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Finish">Finish</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="datedetail" class="col-form-label col-form-label-sm">Revisit</label>
                                    <input type="date" name="revisit" class="form-control form-control-sm">
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4" style="margin-bottom:10px">
        <div class="card">
            <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col">
                <h5 class="mb-0">Request unit</h3>
                </div>
            </div>
            </div>
            <div class="card-body">
                <label for="" class="col-form-label col-form-label-sm">Item</label>
                <div class="input-group inputitem fieldGroup">
                    <!-- <input type="text" class="form-control form-control-sm" name="item[]" placeholder="Nama unit" aria-label="Recipient's username" aria-describedby="button-addon2"> -->
                    <div class="input-group-append" style="margin:0 auto">
                        <button class="btn btn-outline-primary btn-sm addMore" type="button" id="button-addon2" style="width:50px"><i class="fas fa-plus"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= form_close(); ?>
    <div class="input-group fieldGroupCopy" style="display: none;">
        <input type="text" class="form-control form-control-sm" name="item[]" placeholder="Nama unit" aria-label="Recipient's username" aria-describedby="button-addon2" required>
        <div class="input-group-append">
            <button class="btn btn-outline-danger btn-sm remove" type="button" id="button-addon2"><i class="fas fa-trash"></i></button>
        </div>
    </div>

<?= $this->endSection(); ?>