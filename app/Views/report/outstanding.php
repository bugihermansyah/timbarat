<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Report : Outstanding</h6>
            </div>
            <div class="card-body">
            <form method="get" action="">
                <div class="row">
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label>Filter Berdasarkan</label>
                            <select name="filter" id="filter" class="form-control">
                                <option value="">Pilih</option>
                                <option value="1">Per Tanggal</option>
                                <option value="2">Per Bulan</option>
                                <option value="3">Per Tahun</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" id="form-tanggal">
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" name="tanggal" class="form-control  datepicker datetimepicker-input" data-toggle="datetimepicker" data-target=".datepicker" autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-2" id="form-bulan">
                        <div class="form-group">
                            <label>Bulan</label>
                            <select name="bulan" class="form-control">
                                <option value="">Pilih</option>
                                <option value="1">Januari</option>
                                <option value="2">Februari</option>
                                <option value="3">Maret</option>
                                <option value="4">April</option>
                                <option value="5">Mei</option>
                                <option value="6">Juni</option>
                                <option value="7">Juli</option>
                                <option value="8">Agustus</option>
                                <option value="9">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-2" id="form-tahun">
                        <div class="form-group">
                            <label>Tahun</label>
                            <select name="tahun" class="form-control">
                                <option value="">Pilih</option>
                                <?php
                                foreach ($option_tahun as $tahun) :?>
                                <option value="<?= $tahun->tahun ?>"><?= $tahun->tahun ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Tampilkan</button>
                <a href="index.php" class="btn btn-default">Reset Filter</a>
            </form>
            
            <hr>
            <b><?php echo $label; ?></b><br />
            <a href="<?php echo $url_export; ?>" class="btn btn-success btn-sm">EXPORT EXCEL</a><br /><br />
                <!-- <div class="table-responsive"> -->
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Lokasi</th>
                                <th>System</th>
                                <th>Lapor</th>
                                <th>Mulai</th>
                                <th>Selesai</th>
                                <th>PIC</th>
                                <th>Masalah</th>
                                <th>Sebab</th>
                                <th>Solusi</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Lokasi</th>
                                <th>System</th>
                                <th>Lapor</th>
                                <th>Mulai</th>
                                <th>Selesai</th>
                                <th>PIC</th>
                                <th>Masalah</th>
                                <th>Sebab</th>
                                <th>Solusi</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            if( ! empty($laporan)) :
                                $no = 1;
                                foreach ($laporan as $datas) : ?>
                                <tr>
                                    <td width="1%"><?= $no++ ;?></td>
                                    <td><?= esc($datas->lokasi); ?></td>
                                    <td><?= esc($datas->produk); ?></td>
                                    <td><?= esc($datas->tgl_add); ?></td>
                                    <td><?= esc($datas->tglvisit); ?></td>
                                    <td><?= esc($datas->tglvisit); ?></td>
                                    <td><?= esc($datas->pic); ?></td>
                                    <td><?= esc($datas->masalah); ?></td>
                                    <td><?= esc($datas->sebab); ?></td>
                                    <td><?= esc($datas->solusi); ?></td>
                                    <td><?= esc($datas->status); ?></td>
                                </tr>
                            <?php endforeach;
                            endif ?>
                        </tbody>
                    </table>
                <!-- </div> -->
            </div>
        </div>
        <script>
            $('#dataTable').dataTable( {
                "scrollX": true
            } );
        </script>
    <?= $this->endSection(); ?>