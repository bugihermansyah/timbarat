<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Kerusakan Unit</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Unit</th>
                                <th>Jan</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Apr</th>
                                <th>Mei</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ags</th>
                                <th>Sep</th>
                                <th>Okt</th>
                                <th>Nov</th>
                                <th>Des</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Unit</th>
                                <th>Jan</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Apr</th>
                                <th>Mei</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ags</th>
                                <th>Sep</th>
                                <th>Okt</th>
                                <th>Nov</th>
                                <th>Des</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php $no = 1;
                            foreach ($getCountTag1 as $tag1) : ?>
                                <tr>
                                    <td width="1%"><?= $no++ ?></td>
                                    <td><?= esc($tag1->name) ?></td>
                                    <td><?= esc($tag1->januari) ?></td>
                                    <td><?= esc($tag1->februari) ?></td>
                                    <td><?= esc($tag1->maret) ?></td>
                                    <td><?= esc($tag1->april) ?></td>
                                    <td><?= esc($tag1->mei) ?></td>
                                    <td><?= esc($tag1->juni) ?></td>
                                    <td><?= esc($tag1->juli) ?></td>
                                    <td><?= esc($tag1->agustus) ?></td>
                                    <td><?= esc($tag1->september) ?></td>
                                    <td><?= esc($tag1->oktober) ?></td>
                                    <td><?= esc($tag1->november) ?></td>
                                    <td><?= esc($tag1->desember) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    <?= $this->endSection(); ?>