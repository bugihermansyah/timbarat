<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Report : Outstanding</h6>
            </div>
            <div class="card-body">
            <form method="get" action="">
                <div class="row">
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label>Filter Berdasarkan</label>
                            <select name="filter" id="filter" class="form-control">
                                <option value="">Pilih</option>
                                <option value="2">Per Bulan</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label>PIC</label>
                            <select name="pic" id="pic" class="form-control">
                                <option value="">Pilih</option>
                                <option value="Bugi">Bugi</option>
                                <option value="Roma">Roma</option>
                                <option value="Hasbu">Hasbu</option>
                                <option value="Husein">Husein</option>
                                <option value="Dhaysal">Dhaysal</option>
                                <option value="Pradana">Pradana</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" id="form-tanggal">
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" name="tanggal" class="form-control  datepicker datetimepicker-input" data-toggle="datetimepicker" data-target=".datepicker" autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-2" id="form-bulan">
                        <div class="form-group">
                            <label>Bulan</label>
                            <select name="bulan" class="form-control">
                                <option value="">Pilih</option>
                                <option value="1">Januari</option>
                                <option value="2">Februari</option>
                                <option value="3">Maret</option>
                                <option value="4">April</option>
                                <option value="5">Mei</option>
                                <option value="6">Juni</option>
                                <option value="7">Juli</option>
                                <option value="8">Agustus</option>
                                <option value="9">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-2" id="form-tahun">
                        <div class="form-group">
                            <label>Tahun</label>
                            <select name="tahun" class="form-control">
                                <option value="">Pilih</option>
                                <?php
                                foreach ($option_tahun as $tahun) :?>
                                <option value="<?= $tahun->tahun ?>"><?= $tahun->tahun ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Tampilkan</button>
                <a href="index.php" class="btn btn-default">Reset Filter</a>
            </form>
            
            <hr>
            <b><?php echo $label; ?></b><br />
            <a href="#" class="btn btn-success btn-sm">EXPORT EXCEL</a><br /><br />
                <!-- <div class="table-responsive"> -->
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Lokasi</th>
                                <th>Tgl Lapor</th>
                                <th>Pelapor</th>
                                <th>Problem</th>
                                <th>Pic Visit</th>
                                <th>Tgl Visit</th>
                                <th>Status</th>
                                <th>Next</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                                <th>Unit Req</th>
                                <th>Tgl Req</th>
                                <th>Target</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Lokasi</th>
                                <th>Tgl Lapor</th>
                                <th>Pelapor</th>
                                <th>Problem</th>
                                <th>Pic Visit</th>
                                <th>Tgl Visit</th>
                                <th>Status</th>
                                <th>Next</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                                <th>Unit Req</th>
                                <th>Tgl Req</th>
                                <th>Target</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            if( ! empty($laporan)) :
                                $no = 1;
                                foreach ($laporan as $datas) : ?>
                                <tr>
                                    <td width="1%"><?= $no++ ;?></td>
                                    <td><?= esc($datas->lokasi); ?></td>
                                    <td><?= date("d M Y", strtotime($datas->tgl_add)); ?></td>
                                    <td><?= esc($datas->pelapor); ?></td>
                                    <td><?= esc($datas->masalah); ?></td>
                                    <td><?= esc($datas->picvisit); ?></td>
                                    <td><?= ($datas->tglvisit) == null ? '': date("d M Y", strtotime($datas->tglvisit)); ?></td>
                                    <td><?= esc($datas->statusvisit); ?></td>
                                    <td></td>
                                    <td></td>
                                    <td><?= esc($datas->note); ?></td>
                                    <td></td>
                                    <td><?= ($datas->tglrequest) == null ? '':date("d M Y", strtotime($datas->tglrequest)); ?></td>
                                    <td><?= ($datas->tglpengiriman) == null ? '':date("d M Y", strtotime($datas->tglpengiriman)); ?></td>
                                    <td></td>
                                </tr>
                            <?php endforeach;
                            endif ?>
                        </tbody>
                    </table>
                <!-- </div> -->
            </div>
        </div>
        <script>
            var options = {
                "sScrollX": "100%",
                "sScrollXInner": "150%",
                "bScrollCollapse": true,
                responsive : false,
            };

            $(document).ready(function() {
                $('#dataTable').dataTable(options);
            });
        </script>
    <?= $this->endSection(); ?>