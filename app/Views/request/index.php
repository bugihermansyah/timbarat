<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Request unit</h6>
            </div>
            <div class="card-body">
            <a href="" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#addunitModal"><i class="fas fa-plus-square"></i> Add data</a>

            <?php
            $errors = session()->getFlashdata('failed');
            if (!empty($errors)) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-times"></i> Failed</strong> data not added to database.
                    <ul>
                        <?php foreach ($errors as $e) { ?>
                            <li><?= esc($e); ?></li>
                        <?php } ?>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <?php if (session()->getFlashData('success')) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-check"></i> Success</strong> <?= session()->getFlashData('success'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Lokasi</th>
                                <th>Request</th>
                                <th>Pengiriman</th>
                                <th>Pengambilan</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Lokasi</th>
                                <th>Request</th>
                                <th>Pengiriman</th>
                                <th>Pengambilan</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php $no = 1;
                            foreach ($all_data as $datas) : ?>
                                <tr>
                                    <td width="1%"><?= $no++; ?></td>
                                    <td>
                                        <?= esc($datas->lokasi); ?>
                                    </td>
                                    <td><?= date("d M Y", strtotime($datas->tglrequest)); ?></td>
                                    <td><?= esc($datas->tglpengiriman) == NULL ? '<a class="badge badge-warning">Belum pengiriman</a>' : date("d M Y", strtotime($datas->tglpengiriman)); ?></td>
                                    <td><?= esc($datas->tglpengambilan) == NULL ? '<a class="badge badge-warning">Belum pengambilan</a>' : date("d M Y", strtotime($datas->tglpengambilan)); ?></td>
                                    <td class="text-center">
                                        <a href="#" class="btn btn-primary btn-circle btn-sm <?= ($datas->status == "Close") ? 'disabled' : '' ;?>" data-toggle="modal" data-target="#logistikModal<?= $datas->id; ?>">
                                            <i class="fas fa-truck"></i>
                                        </a>
                                        <a href="#" class="btn btn-info btn-circle btn-sm" data-toggle="modal" data-target="#copyModal<?= $datas->id; ?>">
                                            <i class="fas fa-copy"></i>
                                        </a>
                                        <a href="#" class="btn btn-info btn-circle btn-sm" data-toggle="modal" data-target="#viewModal<?= $datas->id; ?>">
                                            <i class="fas fa-search-plus"></i>
                                        </a>
                                        <a href="#" class="btn btn-dark btn-circle btn-sm <?= ($datas->status == "Close") ? 'disabled' : '' ;?>" data-toggle="modal" data-target="#casecloseModal<?= $datas->id; ?>">
                                            <i class="fas fa-eye-slash"></i>
                                        </a>
                                        <a href="#" class="btn btn-danger btn-circle btn-sm" data-toggle="modal" data-target="#deleteModal<?= $datas->id; ?>">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    <!-- Add unit modal -->
    <div class="modal fade" id="addunitModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus-square"></i> Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= form_open('request/add_item'); ?>
                    <?= csrf_field(); ?>
                    <div class="form-group">
                        <label for="PIC">Lokasi</label>
                        <select name="id_lokasi" id="select2" class="form-control" style="width: 100%" required>
                            <option value="">- Select -</option>
                            <?php foreach($all_lokasi as $lokasi) : ?>
                                <option value="<?= $lokasi->lokasi_id;?>"><?= esc($lokasi->lokasi);?></option>
                            <?php endforeach; ?>
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="PIC">Masalah <i>Lokasi : Masalah | PIC Visit | Tanggal Visit</i></label>
                        <select name="id_laporandetail" id="select3" class="form-control" style="width: 100%" required>
                            <option value="">- Select -</option>
                            <?php foreach($all_laporan as $laporan) : ?>
                                <option value="<?= $laporan->laporandetail_id;?>"><?= esc($laporan->lokasi);?> : <?= esc($laporan->masalah);?> | <?= esc($laporan->picvisit);?> | <?= date("d M Y", strtotime($laporan->tglvisit));?></option>
                            <?php endforeach; ?>
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="Lokasi">Tanggal Request</label>
                        <input type="date" name="tglrequest" id="Lokasi" class="form-control" required>
                    </div>
                    <label for="">Item</label>
                    <div class="input-group inputitem fieldGroup">
                        <input type="text" class="form-control" name="item[]" placeholder="Nama unit" aria-label="Recipient's username" aria-describedby="button-addon2" required>
                        <div class="input-group-append">
                            <button class="btn btn-outline-primary addMore" type="button" id="button-addon2"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="input-group fieldGroupCopy" style="display:none">
        <input type="text" class="form-control" name="item[]" placeholder="Nama unit" aria-label="Recipient's username" aria-describedby="button-addon2" required>
        <div class="input-group-append">
            <button class="btn btn-outline-danger remove" type="button" id="button-addon2"><i class="fas fa-trash"></i></button>
        </div>
    </div>

    <!-- Update Logistik -->
    <?php foreach ($all_data as $datas) : ?>
        <div class="modal fade" id="logistikModal<?= $datas->id; ?>" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-truck"></i> Update Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab<?= $datas->id; ?>" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab<?= $datas->id; ?>" data-toggle="tab" href="#nav-home<?= $datas->id; ?>" role="tab" aria-controls="nav-home<?= $datas->id; ?>" aria-selected="true">Pengiriman</a>
                                <a class="nav-item nav-link <?= $datas->tglpengiriman == null ? "disabled" : "";?>" id="nav-profile-tab<?= $datas->id; ?>" data-toggle="tab" href="#nav-profile<?= $datas->id; ?>" role="tab" aria-controls="nav-profile" aria-selected="false">Pengambilan</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent<?= $datas->id; ?>">
                            <div class="tab-pane fade show active" id="nav-home<?= $datas->id; ?>" role="tabpanel" aria-labelledby="nav-home-tab">
                                <?= form_open('request/update_pengiriman/' . $datas->id); ?>
                                <?= csrf_field(); ?>
                                <input type="hidden" name="id" value="<?= $datas->id; ?>">
                                <br>
                                <div class="form-group">
                                    <label for="name">Tanggal Pengiriman</label>
                                    <input type="date" name="tglpengiriman" id="tglpengiriman" class="form-control" value="<?= esc($datas->tglpengiriman);?>" required>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                </div>
                                <?= form_close(); ?>
                            </div>
                            <div class="tab-pane fade" id="nav-profile<?= $datas->id; ?>" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <?= form_open('request/update_pengambilan/' . $datas->id); ?>
                                <?= csrf_field(); ?>
                                <input type="hidden" name="id" value="<?= $datas->id; ?>">
                                <br>
                                <table class="table table-sm table-striped">
                                    <thead>
                                    <tr>
                                        <th>Unit</th>
                                        <th>Terkirim</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($all_unit as $unit) : ?>
                                            <?php if ($unit->id_request == $datas->id) : ?>
                                        <tr>                                    
                                            <input type="hidden" name="id[]" value="<?= esc($unit->requestdetail_id); ?>">
                                            <td><?= esc($unit->item); ?></td>
                                            <td>
                                                <select name="status[]">
                                                    <?php if ($stat = $unit->status ?? $unit->status) : ?>
                                                    <option value="Y" <?= $stat == 'Y' ? 'selected' : null ;?>>Y</option>
                                                    <option value="N" <?= $stat == 'N' ? 'selected' : null ;?>>N</option>
                                                    <?php endif ;?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <hr>
                                <div class="form-group">
                                    <label for="name">Tanggal Pengambilan</label>
                                    <input type="date" name="tglpengambilan" id="tglpengambilan" class="form-control" value="<?= $datas->tglpengambilan; ?>" required>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <!-- View Data -->
    <?php foreach ($all_data as $datas) : ?>
        <div class="modal fade" id="viewModal<?= $datas->id; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-list-ol"></i> View Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center"><?= $datas->lokasi ;?></h5>
                        <table class="table table-sm table-striped">
                            <thead>
                            <tr>
                                <th>Unit</th>
                                <th>Terkirim</th>
                                <th>Note</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_unit as $unit) : ?>
                                    <?php if ($unit->id_request == $datas->id) : ?>
                                <tr>
                                    <input type="hidden" name="id" value="<?= esc($unit->requestdetail_id); ?>">
                                    <td><?= esc($unit->item); ?></td>
                                    <td><input type="checkbox" name="" value="Y" class="form-check-input" style="margin:0 auto" <?= $unit->status == "N" ? '' : 'checked' ;?> disabled></td>
                                    <td><?= $unit->status == "N" ? '<a class="badge badge-warning">belum terkirim</a>' : '<a class="badge badge-success">terkirim</a>' ;?></td>
                                </tr>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <!-- Clipboard Data -->
    <?php foreach ($all_data as $datas) : ?>
        <div class="modal fade" id="copyModal<?= $datas->id; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-copy"></i> Copy Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center"><?= $datas->lokasi ;?></h5>
                        <hr>
                            <textarea class="form-control" rows="7" id="bar<?= $datas->id; ?>">Req <?= $datas->lokasi ;?> :&#013;<?php foreach ($all_unit as $unit) : ?><?php if($unit->id_request == $datas->id) : ?>- <?= esc($unit->item); ?>&#013;<?php endif; ?><?php endforeach; ?>PIC Support : <?= $datas->pic ;?>&#013;PIC Lokasi : <?= $datas->contact ;?></textarea>

                        <div class="modal-footer">
                        <button
                            class="btn btn-info btn-sm"
                            data-clipboard-action="copy"
                            data-clipboard-target="#bar<?= $datas->id; ?>"
                            >
                            clipboard
                        </button>
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    
    <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.8/dist/clipboard.min.js"></script>
    <script>
      var clipboard = new ClipboardJS('.btn');

      clipboard.on('success', function (e) {
        console.log(e);
      });

      clipboard.on('error', function (e) {
        console.log(e);
      });
    </script>

    <!-- Delete modal -->
    <?php foreach ($all_data as $datas) : ?>
    <div class="modal fade" id="deleteModal<?= $datas->id; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-exclamation-circle"></i> Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?= form_open('request/delete_data/' . $datas->id); ?>
                        <?= csrf_field(); ?>
                        <input type="hidden" name="id" value="<?= $datas->id; ?>">
                        <p>Lokasi : <?= $datas->lokasi; ?></p>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <!-- Case Close Modal-->
    <?php foreach ($all_data as $datas) : ?>
    <div class="modal fade" id="casecloseModal<?= $datas->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <?= form_open('request/update_status/' . $datas->id); ?>
            <input type="hidden" name="status" value="Close">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="casecloseModalLabel">Request close?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Request unit akan di "Close".</div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
    <?php endforeach; ?>
    <?= $this->endSection(); ?>