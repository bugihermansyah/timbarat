<?= $this->extend('layout/templates'); ?>

<?= $this->Section('content'); ?>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Lokasi</h6>
            </div>
            <div class="card-body">
            <a href="" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#addModal"><i class="fas fa-plus-square"></i> Add data</a>

            <?php
            $errors = session()->getFlashdata('failed');
            if (!empty($errors)) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-times"></i> Failed</strong> data not added to database.
                    <ul>
                        <?php foreach ($errors as $e) { ?>
                            <li><?= esc($e); ?></li>
                        <?php } ?>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <?php if (session()->getFlashData('success')) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-check"></i> Success</strong> <?= session()->getFlashData('success'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-sm nowrap" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Group</th>
                                <th>Lokasi</th>
                                <th>Produk</th>
                                <th>Status</th>
                                <th>BAP</th>
                                <th>PIC</th>
                                <th>BD</th>
                                <th>Kontak</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Group</th>
                                <th>Lokasi</th>
                                <th>Produk</th>
                                <th>Status</th>
                                <th>BAP</th>
                                <th>PIC</th>
                                <th>BD</th>
                                <th>Kontak</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php $no = 1;
                            foreach ($all_data as $datas) : ?>
                                <tr>
                                    <td width="1%"><?= $no++ ;?></td>
                                    <td><?= esc($datas->perusahaan_name); ?></td>
                                    <td><?= esc($datas->lokasi); ?></td>
                                    <td><?= esc($datas->produk); ?></td>
                                    <td><?= esc($datas->status_sewa); ?></td>
                                    <td><?= $datas->tgl_bap == null ? "" : date("d M Y", strtotime($datas->tgl_bap)); ?>
                                    <td><?= esc($datas->pic); ?></td>
                                    <td><?= esc($datas->pic_name); ?></td>
                                    <td><?= esc($datas->contact); ?></td>
                                    <td class="text-center" width="10%">
                                        <a href="#" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#updateModal<?= $datas->id; ?>">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    <!-- Add modal -->
    <div class="modal fade" id="addModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus-square"></i> Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= form_open('lokasi/add_data'); ?>
                    <?= csrf_field(); ?>
                    <div class="form-group">
                        <label for="Lokasi">Nama Lokasi</label>
                        <input type="text" name="lokasi" id="Lokasi" class="form-control" placeholder="Nama lokasi" required>
                    </div>
                    <div class="form-group">
                        <label for="perusahaan">Perusahaan</label>
                        <select name="id_perusahaan" id="select2" class="form-control" style="width:100%">
                                <option value="">- Select -</option>
                            <?php foreach ($all_perusahaan as $perusahaan) : ?>
                                <option value="<?= $perusahaan->perusahaan_id ;?>">
                                    <?= $perusahaan->name ;?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="PIC">Produk</label>
                        <select name="produk" id="PIC" class="form-control" required>
                            <option value="">- Select -</option>
                            <?php foreach($getProduk as $produk) : ?>
                            <option value="<?= $produk->produk_id ?>"><?= $produk->produk ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="PIC">PIC</label>
                        <select name="pic" id="PIC" class="form-control" required>
                            <option value="">- Select -</option>
                            <?php
                            foreach($getPic as $pic) : ?>
                            <option value="<?= $pic->name ?>"><?= $pic->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="contact">PIC Lokasi</label>
                        <input type="text" name="contact" id="contact" class="form-control" placeholder="Nama - No. Tlp" >
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Update modal -->
    <?php foreach ($all_data as $datas) : ?>
        <div class="modal fade" id="updateModal<?= $datas->id; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-edit"></i> Update Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?= form_open('lokasi/update_data/' . $datas->id); ?>
                        <?= csrf_field(); ?>
                        <input type="hidden" name="id" value="<?= $datas->id; ?>">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="lokasi" id="lokasi" class="form-control" value="<?= $datas->lokasi; ?>"  required>
                        </div>
                        <div class="form-group">
                            <label for="perusahaan">Perusahaan</label>
                            <select name="id_perusahaan" id="" class="form-control">
                                <option value="">- Kosong -</option>
                                <?php if ($dept = $datas->id_perusahaan ?? $datas->id_perusahaan) : ?>
                                    <option value="<?= $datas->perusahaan_id ;?>" <?= $dept == $datas->perusahaan_id ? 'selected' : null ?>>
                                        <?= $datas->perusahaan_name ;?>
                                    </option>
                                <?php endif ;?>
                                <?php foreach ($all_perusahaan as $perusahaan) : ?>
                                    <option value="<?= $perusahaan->perusahaan_id ;?>">
                                        <?= $perusahaan->name ;?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="dept">PIC</label>
                            <select name="pic" id="pic" class="form-control" required>
                                <?php if ($dept = $datas->pic ?? $datas->pic) : ?>
                                    <option value="<?= $datas->pic ?>" <?= $dept == $datas->pic ? 'selected' : null ?>>
                                       <?= $datas->pic ?>
                                    </option>
                                <?php endif ?>
                                <?php foreach($getPic as $pic) :?>
                                    <option value="<?= $pic->name ?>"><?= $pic->name ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="contact">PIC Lokasi</label>
                            <input type="text" name="contact" id="contact" class="form-control" value="<?= $datas->contact; ?>" placeholder="Nama - No. Tlp" >
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <?= $this->endSection(); ?>