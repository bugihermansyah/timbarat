<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override(function(){
    return view('404');
});
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Dashboard::index');
$routes->get('/export-pdf', 'Export::export_pdf');
$routes->get('/export-excel', 'Export::export_excel');

$routes->add('/laporan/aksi/(:num)', 'Laporan::aksi/$1');
$routes->add('/laporan/timeline/(:num)', 'Laporan::timeline/$1');
$routes->add('/laporan/edit/(:num)', 'Laporan::edit/$1');
$routes->get('/laporan/(:segment)/delete', 'Laporan::delete/$1');
$routes->add('/laporan/update_open/(:num)', 'Laporan::update_open/$1');
$routes->add('/laporan/edit_laporan/(:num)', 'Laporan::edit_laporan/$1');
$routes->add('/report/kerusakan', 'Kerusakan::index');
$routes->add('/report/outstanding', 'Outstanding::index');
$routes->add('/report/lokasiblok', 'Visitpic::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
