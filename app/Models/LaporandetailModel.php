<?php

namespace App\Models;

use CodeIgniter\Model;

class LaporandetailModel extends Model
{
    protected $db, $builder;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('laporandetail'); // tabel laporan  
    }

    // func select all data or by id
    public function select_data($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->get()->getResultObject();
        }

        return $this->builder->select()
                             ->join('laporan', 'laporandetail.id_laporan = laporan.laporan_id')
                             ->getWhere(['laporan_id' => $id])->getResultObject();
    }

    public function count_onsite($id = FALSE)
    {
        return $this->builder->select()
                             ->join('laporan', 'laporandetail.id_laporan = laporan.laporan_id')
                             ->where('onsite', '1')
                             ->where(['laporan_id' => $id])
                             ->countAllResults();
    }

    public function count_remote($id = FALSE)
    {
        return $this->builder->select()
                             ->join('laporan', 'laporandetail.id_laporan = laporan.laporan_id')
                             ->where('onsite', '0')
                             ->where(['laporan_id' => $id])
                             ->countAllResults();
    }

    public function getLastData($id)
    {
        return $this->builder->select()
                             ->join('laporan', 'laporandetail.id_laporan = laporan.laporan_id')
                             ->orderBy('laporandetail_id', 'DESC')
                             ->limit(1)
                             ->getWhere(['laporan_id' => $id])
                             ->getRow();
    }

    // func insert data to db
    public function add_laporandetail($data)
    {
        $this->builder->insert($data);
    }

    // func delete data from db
    public function delete_data($id)
    {
        $this->builder->where('laporandetail_id', $id);
        $this->builder->delete();
    }

    // func update data from db
    public function update_data($id, $data)
    {
        $this->builder->where('laporandetail_id', $id);
        $this->builder->update($data);
    }
}
