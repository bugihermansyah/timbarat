<?php

namespace App\Models;

use CodeIgniter\Model;

class LokasiModel extends Model
{
    protected $db, $builder;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('lokasi'); // tabel employees  
    }

    // func select all data or by id
    public function select_data($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->select('*,lokasi_id as id, perusahaan.name as perusahaan_name ')
                                 ->join('perusahaan', 'lokasi.id_perusahaan = perusahaan.perusahaan_id', 'left')
                                 ->join('produk', 'lokasi.id_produk = produk.produk_id')
                                 ->orderBy('lokasi', 'asc')
                                 ->get()->getResultObject();
        }

        return $this->builder->getWhere(['id' => $id])->getRow();
    }
    
    // func select all data or by id
    public function select_lokasi($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->get()->getResultObject();
        }

        return $this->builder->getWhere(['id' => $id])->getRow();
    }

    // func insert data to db
    public function add_data($data)
    {
        $this->builder->insert($data);
    }

    // func delete data from db
    public function delete_data($id)
    {
        $this->builder->where('lokasi_id', $id);
        $this->builder->delete();
    }

    // func update data from db
    public function update_data($id, $data)
    {
        $this->builder->where('lokasi_id', $id);
        $this->builder->update($data);
    }

    public function select_count_lokasi_all()
    {
        return $this->builder->countAllResults();
    }
}
