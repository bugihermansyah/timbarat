<?php

namespace App\Models;

use CodeIgniter\Model;

class TagModel extends Model
{
    protected $db, $builder;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('tags'); // tabel employees  
    }

    // func select all data or by id
    public function getTags($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->get()->getResultObject();
        }

        return $this->builder->join('tags_detail', 'tags.tag_id = tags_detail.id_tag')
                             ->join('laporan', 'tags_detail.id_laporan = laporan.laporan_id', 'right')
                             ->getWhere(['laporan_id' => $id])->getResultObject();
    }

    // func insert data to db
    public function add_data($data)
    {
        $this->builder->insert($data);
    }

    // func delete data from db
    public function delete_data($id)
    {
        $this->builder->where('id', $id);
        $this->builder->delete();
    }

    // func update data from db
    public function update_data($id, $data)
    {
        $this->builder->where('id', $id);
        $this->builder->update($data);
    }

    public function getCountTag1()
    {
        $db = \Config\Database::connect();
        $query = $db->query('select a.name,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 1
        ) as januari,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 2
        ) as februari,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 3
        ) as maret,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 4
        ) as april,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 5
        ) as mei,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 6
        ) as juni,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 6
        ) as juni,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 7
        ) as juli,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 8
        ) as agustus,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 9
        ) as september,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 10
        ) as oktober,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 11
        ) as november,
        (
          SELECT COUNT(b.id_tag) FROM tags_detail b 
          INNER JOIN laporan c ON b.id_laporan = c.laporan_id
          WHERE b.id_tag = a.tag_id && MONTH(c.tgl_add) = 12
        ) AS desember
    FROM tags a');
    return $query->getResult();
    }
}
