<?php

namespace App\Models;

use CodeIgniter\Model;

class RequestdetailModel extends Model
{
    protected $db, $builder;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('requestdetail'); // tabel requestdetail
    }

    // func select all data or by id
    public function select_data($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->select('*,request_id as id ')
                                 ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                                 ->orderBy('id', 'DESC')->get()->getResultObject();
        }

        return $this->builder->getWhere(['id' => $id])->getRow();
    }    // func select all data or by id
    
    public function select_unit($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->get()->getResultObject();
        }

        return $this->builder->getWhere(['id_request' => $id])->getRow();
    }

    // func insert data to db
    public function add_item($data)
    {
        $this->builder->insertBatch($data);
    }

    // func delete data from db
    public function delete_data($id)
    {
        $this->builder->where('id_request', $id);
        $this->builder->delete();
    }

    // func update data from db
    public function update_data($id, $data)
    {
        $this->builder->where('id_request', $id);
        $this->builder->update($data);
    }

    // func update status from db
    public function update_status($data, $id)
    {
        // $udata = [
        //     'status' => 'N'
        // ];
        // $this->builder->where('id_request', $id);
        // $this->builder->update($udata);
        $this->builder->updateBatch($data, 'requestdetail_id');
    }
}
