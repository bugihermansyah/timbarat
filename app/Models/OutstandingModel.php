<?php

namespace App\Models;

use CodeIgniter\Model;

class OutstandingModel extends Model
{
    protected $db, $builder;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('laporan');
    }

    public function getByDate($date){
        return $this->builder->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->join ('produk', 'lokasi.id_produk = produk.produk_id')
                             ->join('laporandetail', 'laporan.laporan_id = laporandetail.id_laporan', 'left')
                             ->where('DATE(tgl_add)', $date) // Tambahkan where tanggal nya
                             ->get()->getResultObject(); // Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
	}
    
	public function getByMonth($month, $year){
        return $this->builder->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->join ('produk', 'lokasi.id_produk = produk.produk_id')
                             ->join('laporandetail', 'laporan.laporan_id = laporandetail.id_laporan', 'left')
                             ->where('MONTH(tgl_add)', $month) // Tambahkan where bulan
                             ->where('YEAR(tgl_add)', $year) // Tambahkan where tahun
                             ->get()->getResultObject(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}
    
	public function getByYear($year){
        return $this->builder->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->join ('produk', 'lokasi.id_produk = produk.produk_id')
                             ->join('laporandetail', 'laporan.laporan_id = laporandetail.id_laporan', 'left')
                             ->where('YEAR(tgl_add)', $year)
                             ->get()->getResultObject(); // Tampilkan data transaksi sesuai tahun yang diinput oleh user pada filter
	}
    
	public function view_all(){
		// return $this->db->get('laporan')->result(); // Tampilkan semua data transaksi
	}
    
    public function getYear(){
        return $this->builder->select('YEAR(tgl_add) AS tahun') // Ambil Tahun dari field tgl
                             ->orderBy('YEAR(tgl_add)') // Urutkan berdasarkan tahun secara Ascending (ASC)
                             ->groupBy('YEAR(tgl_add)') // Group berdasarkan tahun pada field tgl
                             ->get()->getResultObject(); // Ambil data pada tabel transaksi sesuai kondisi diatas
    }
}
