<?php

namespace App\Models;

use CodeIgniter\Model;

class LaporanModel extends Model
{
    protected $db, $builder;
    protected $primaryKey = "laporan_id";

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('laporan'); // tabel laporan  
    }

    // func select all data or by id
    public function select_laporan()
    {
        return $this->builder->select('*')
                                 ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                                 ->where('open', 'Open')
                                 ->get()->getResultObject();
    }

    public function getLaporanDetailByOpen()
    {
        return $this->builder->select('laporandetail_id, lokasi, masalah, laporandetail.pic AS picvisit, tglvisit')
                             ->join ('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->join ('laporandetail','laporandetail.id_laporan = laporan.laporan_id')
                             ->where('open','Open')
                             ->get()->getResultObject();
    }

    // func select all data or by id
    public function laporan_lokasi($id = FALSE)
    {
        return $this->builder->select('*')
                                 ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                                 ->where('open', 'Open')
                                 ->get()->getResultObject();
    }

    // func select all data or by id
    public function select_data($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->select('*, laporan.laporan_id as id, laporan.created_at as lap_created_at')
                                 ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                                 ->join('users', 'laporan.id_user = users.id', 'left')
                                 ->orderBy('open', 'Open')
                                 ->get()->getResultObject();
        }
        return $this->builder->select('*, laporan.laporan_id as id, laporan.created_at as lap_created_at')
                                ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                                ->join('users', 'laporan.id_user = users.id', 'left')
                                ->getWhere(['laporan_id' => $id])->getRow();
    }

    public function select_count_open_all()
    {
        return $this->builder->select('*, laporan.laporan_id as id')
                             ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->where('open', 'Open')
                             ->countAllResults();
    }

    public function select_count_open_bugi()
    {
        return $this->builder->select('*, laporan.laporan_id as id')
                             ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->where('open', 'Open')
                             ->like('pic', 'Bugi')
                             ->countAllResults();
    }
    public function select_count_open_roma()
    {
        return $this->builder->select('*, laporan.laporan_id as id')
                             ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->where('open', 'Open')
                             ->like('pic', 'Roma')
                             ->countAllResults();
    }
    public function select_count_open_fajar()
    {
        return $this->builder->select('*, laporan.laporan_id as id')
                             ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->where('open', 'Open')
                             ->like('pic', 'Fajar')
                             ->countAllResults();
    }
    public function select_count_open_hasbu()
    {
        return $this->builder->select('*, laporan.laporan_id as id')
                             ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->where('open', 'Open')
                             ->like('pic', 'Hasbu')
                             ->countAllResults();
    }
    public function select_count_open_husein()
    {
        return $this->builder->select('*, laporan.laporan_id as id')
                             ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->where('open', 'Open')
                             ->like('pic', 'Husein')
                             ->countAllResults();
    }
    public function select_count_open_dhaysal()
    {
        return $this->builder->select('*, laporan.laporan_id as id')
                             ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->where('open', 'Open')
                             ->like('pic', 'Dhaysal')
                             ->countAllResults();
    }
    public function select_count_open_dana()
    {
        return $this->builder->select('*, laporan.laporan_id as id')
                             ->join('lokasi', 'laporan.id_lokasi = lokasi.lokasi_id')
                             ->where('open', 'Open')
                             ->like('pic', 'Dana')
                             ->countAllResults();
    }

    // func insert data to db
    public function add_data($data)
    {
        $this->builder->insertBatch($data);
    }

    // func delete data from db
    public function delete_data($id)
    {
        $this->builder->where('laporan_id', $id);
        $this->builder->delete();
    }

    // func update data from db
    public function update_data($id, $data)
    {
        $this->builder->where('laporan_id', $id);
        $this->builder->update($data);
    }
}
