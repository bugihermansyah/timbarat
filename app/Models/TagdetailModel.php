<?php

namespace App\Models;

use CodeIgniter\Model;

class TagdetailModel extends Model
{
    protected $db, $builder;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('tags_detail');
    }

    // func select all data or by id
    public function getTags($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->get()->getResultObject();
        }

        return $this->builder->getWhere(['id_laporan' => $id])->getResultObject();
    }

    // func insert data to db
    public function add_data($data)
    {
        $this->builder->insert($data);
    }

    // func delete data from db
    public function delete_data($id)
    {
        $this->builder->where('id', $id);
        $this->builder->delete();
    }

    // func update data from db
    public function update_data($id, $data)
    {
        $this->builder->where('id', $id);
        $this->builder->update($data);
    }
}
