<?php

namespace App\Models;

use CodeIgniter\Model;

class RequestModel extends Model
{
    protected $db, $builder;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('request'); // tabel request  
    }

    // func select all data or by id
    public function select_data($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->select('*, request_id as id')
                                 ->join('laporan', 'request.id_laporan = laporan.laporan_id', 'left')
                                 ->join('laporandetail', 'laporan.laporan_id = laporandetail.id_laporan','left')
                                 ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id', 'left')
                                 ->orderBy('request.status', 'ASC')
                                 ->orderBy('tglpengiriman', 'ASC')
                                 ->get()->getResultObject();
        }

        return $this->builder->getWhere(['request_id' => $id])->getRow();
    }

    public function getRequestTimeline($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->get()->getResultObject();
        }

        return $this->builder->select()
                             ->join('laporan', 'request.id_laporan = laporan.laporan_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->getWhere(['laporan_id' => $id])->getResultObject();
    }

    public function select_jatuhtempo($id = FALSE)
    {
        if ($id == FALSE) {
            return $this->builder->select('*, tglpengiriman, requestdetail.status as dstatus, request.status as rstatus, date_add(tglpengiriman, interval 5 day) as jatuh_tempo, datediff(date_add(tglpengiriman, interval 5 day), curdate()) as selisih')
                                 ->join('laporan', 'request.id_laporan = laporan.laporan_id')
                                 ->join('lokasi', 'lokasi.lokasi_id = laporan.id_lokasi')
                                 ->join('requestdetail', 'requestdetail.id_request = request.request_id')
                                 ->where('request.status', 'Open')
                                 ->where('requestdetail.status', 'N')
                                 ->orderBy('selisih', 'ASC')->get()->getResultObject();
        }

        return $this->builder->getWhere(['id' => $id])->getRow();
    }

    // func insert unit to db
    public function add_item($data)
    {
        $this->builder->insertBatch($data);
    }

    // func insert data to db
    public function add_data($data)
    {
        $this->builder->insert($data);
    }
    
    // func update pengiriman data to db
    public function update_pengiriman($id, $data)
    {
        
        $this->builder->where('request_id', $id);
        $this->builder->update($data);
    }

    // func update pengiriman data to db
    public function update_pengambilan($id, $data)
    {
        
        $this->builder->where('request_id', $id);
        $this->builder->update($data);
    }

    // func delete data from db
    public function delete_data($id)
    {
        $this->builder->where('request_id', $id);
        $this->builder->delete();
    }

    // func update data from db
    public function update_data($id, $data)
    {
        $this->builder->where('request_id', $id);
        $this->builder->update($data);
    }

    public function select_count_req_all()
    {
        return $this->builder->select('*')
                             ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->where('request.status', 'Open')
                             ->where('requestdetail.status', 'N')
                             ->countAllResults();
    }

    public function select_count_req_bugi()
    {
        return $this->builder->select('*')
                             ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->where('request.status', 'Open')
                             ->where('requestdetail.status', 'N')
                             ->like('pic', 'Bugi')
                             ->countAllResults();
    }

    public function select_count_req_roma()
    {
        return $this->builder->select('*')
                             ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->where('request.status', 'Open')
                             ->where('requestdetail.status', 'N')
                             ->like('pic', 'Roma')
                             ->countAllResults();
    }

    public function select_count_req_fajar()
    {
        return $this->builder->select('*')
                             ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->where('request.status', 'Open')
                             ->where('requestdetail.status', 'N')
                             ->like('pic', 'Fajar')
                             ->countAllResults();
    }

    public function select_count_req_hasbu()
    {
        return $this->builder->select('*')
                             ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->where('request.status', 'Open')
                             ->where('requestdetail.status', 'N')
                             ->like('pic', 'Hasbu')
                             ->countAllResults();
    }

    public function select_count_req_husein()
    {
        return $this->builder->select('*')
                             ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->where('request.status', 'Open')
                             ->where('requestdetail.status', 'N')
                             ->like('pic', 'Husein')
                             ->countAllResults();
    }

    public function select_count_req_dhaysal()
    {
        return $this->builder->select('*')
                             ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->where('request.status', 'Open')
                             ->where('requestdetail.status', 'N')
                             ->like('pic', 'Dhaysal')
                             ->countAllResults();
    }

    public function select_count_req_dana()
    {
        return $this->builder->select('*')
                             ->join('lokasi', 'request.id_lokasi = lokasi.lokasi_id')
                             ->join('requestdetail', 'request.request_id = requestdetail.id_request')
                             ->where('request.status', 'Open')
                             ->where('requestdetail.status', 'N')
                             ->like('pic', 'Dana')
                             ->countAllResults();
    }
}
