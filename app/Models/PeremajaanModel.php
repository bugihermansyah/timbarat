<?php
namespace App\Models;
use CodeIgniter\Model;

class PeremajaanModel extends Model
{
	protected $table         = 'peremajaan';
	protected $primaryKey    = 'peremajaan_id';
	protected $returnType    = 'array';
	protected $allowedFields = ['peremajaan_id','id_lokasi','tgl_req', 'jenis_peremajaan', 'status','note'];
	protected $useTimestamps = true;
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
	protected $db, $builder;

	function getLokasi()
	{
		$builder = $this->db->table('peremajaan');
		$builder->select('*');
		$builder->join('lokasi', 'peremajaan.id_lokasi = lokasi.lokasi_id');
		$query = $builder->get();
		return $query->getResultArray();
	}
}

