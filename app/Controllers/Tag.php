<?php

namespace App\Controllers;

use App\Models\TagModel;

class Tag extends BaseController
{
    protected $tagModel;

    public function __construct()
    {
        $this->tagModel = new tagModel();   
        helper('form'); 
    }

    public function index()
    {
        $data = [
            'title' => 'Tags',
            'all_data' => $this->lokasiModel->select_data()
        ];

        return view('lokasi/index', $data);
    }

    public function add_data()
    {
        // validation
        $rules = $this->validate([
            'name' => 'required',
            'pic' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $data = [
            'id_perusahaan' => $this->request->getPost('id_perusahaan'),
            'name' => $this->request->getPost('name'),
            'pic' => $this->request->getPost('pic'),
            'id_user' => user_id(),
            'contact' => $this->request->getPost('contact'),
            'created_at' => date('Y-m-d H:i:s')
        ];

        $this->lokasiModel->add_data($data);
        session()->setFlashData('success', 'data has been added to database.');
        return redirect()->back();
    }

    public function delete_data($id)
    {
        $this->lokasiModel->delete_data($id);
        session()->setFlashData('success', 'data has been deleted from database.');
        return redirect()->back();
    }

    public function update_data($id)
    {
        // validation
        $rules = $this->validate([
            'name' => 'required',
            'pic' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $data = [
            'name' => $this->request->getPost('name'),
            'pic' => $this->request->getPost('pic'),
            'contact' => $this->request->getPost('contact')
        ];

        $this->lokasiModel->update_data($id, $data);
        session()->setFlashData('success', 'data has been updated from database.');
        return redirect()->back();
    }
}
