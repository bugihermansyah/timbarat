<?php

namespace App\Controllers;

use App\Models\TagModel;

class Kerusakan extends BaseController
{
    protected $tagModel;

    public function __construct()
    {
        $this->tagModel = new tagModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Export : Kerusakan',
            'getCountTag1' => $this->tagModel->getCountTag1(),
            'getTag' => $this->tagModel->getTags()
        ];

        return view('report/kerusakan', $data);
    }
}
