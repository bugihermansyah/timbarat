<?php
namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PeremajaanModel;
use App\Models\LokasiModel;

class Peremajaan extends BaseController
{
	protected $peremajaan;
    protected $lokasi;

	public function __construct()
	{
		$this->peremajaan = new PeremajaanModel();
		$this->lokasi = new LokasiModel();
	}

	public function index()
	{
		$data = [
			'title' => 'Peremajaan'
		];

		return view('peremajaan/index', $data);
	}

	public function getData()
	{
		if ($this->request->isAJAX()) {
			$data = [
				'dataPeremajaan' => $this->peremajaan->getLokasi()
			];

			$result = [
				'output' => view('peremajaan/view_data', $data)
			];

			echo json_encode($result);
		} else {
			exit('404 Not Found');
		}
	}

	public function getModal()
	{
		if ($this->request->isAJAX()) {
            $data = [
                'dataLokasi' =>$this->lokasi->select_lokasi()
            ];

			$result = [
				'output' => view('peremajaan/view_modal', $data)
			];

			echo json_encode($result);
		} else {
			exit('404 Not Found');
		}
	}

	public function saveData()
	{
		if ($this->request->isAJAX()) {
			$validation = \Config\Services::validation();

			$rules = $this->validate([
				'id_lokasi' => [
					'label' => 'lokasi',
					'rules' => 'required',
				],
				'jenis_peremajaan' => [
					'label' => 'jenis peremajaan',
					'rules' => 'required|min_length[3]',
				]
                ,
				'tgl_req' => [
					'label' => 'tanggal request',
					'rules' => 'required',
				],
				'status' => [
					'label' => 'status',
					'rules' => 'required',
				]
				// ,
				// 'note' => [
				// 	'label' => 'keterangan',
				// 	'rules' => 'required',
				// ]
			]);

			if (!$rules) {
				$result = [
					'error' => [
						'id_lokasi' => $validation->getError('id_lokasi'),
						'tgl_req' => $validation->getError('tgl_req'),
						'jenis_peremajaan' => $validation->getError('peremajaan'),
						'status' => $validation->getError('status'),
						'note' => $validation->getError('note')
					]
				];
			} else {
				$this->peremajaan->insert([
					'id_lokasi' => strip_tags($this->request->getPost('id_lokasi')),
					'tgl_req' => strip_tags($this->request->getPost('tgl_req')),
					'jenis_peremajaan' => strip_tags($this->request->getPost('jenis_peremajaan')),
					'status' => strip_tags($this->request->getPost('status')),
					'note' => strip_tags($this->request->getPost('note'))
				]);

				$result = [
					'success' => 'Data has been added to database'
				];
			}
			echo json_encode($result);
		} else {
			exit('404 Not Found');
		}
	}

	public function getModalEdit()
	{
		if ($this->request->isAJAX()) {
			$id = $this->request->getVar('peremajaan_id');

			$row = $this->peremajaan->find($id);

			$data = [
				'peremajaan_id'	=> $row['peremajaan_id'],
				'id_lokasi'	=> $row['id_lokasi'],
				'tgl_req'	=> $row['tgl_req'],
				'jenis_peremajaan'	=> $row['jenis_peremajaan'],
				'status'	=> $row['status'],
				'note'	=> $row['note'],
				'dataLokasi' =>$this->lokasi->select_lokasi()
			];

			$result = [
				'output' => view('peremajaan/view_modal_edit', $data)
			];

			echo json_encode($result);
		} else {
			exit('404 Not Found');
		}
	}

	public function updateData()
	{
		if ($this->request->isAJAX()) {
			$validation = \Config\Services::validation();

			$rules = $this->validate([
				'id_lokasi' => [
					'label' => 'lokasi',
					'rules' => 'required',
				],
				'tgl_req' => [
					'label' => 'tanggal request',
					'rules' => 'required',
				],
				'jenis_peremajaan' => [
					'label' => 'jenis peremajaan',
					'rules' => 'required|min_length[3]',
				],
				'status' => [
					'label' => 'status',
					'rules' => 'required',
				],
				'note' => [
					'label' => 'keterangan',
					'rules' => 'required',
				]
			]);

			if (!$rules) {
				$result = [
					'error' => [
						'id_lokasi' => $validation->getError('id_lokasi'),
						'tgl_req' => $validation->getError('tgl_req'),
						'jenis_peremajaan' => $validation->getError('jenis_peremajaan'),
						'status' => $validation->getError('status'),
						'note' => $validation->getError('note')
					]
				];
			} else {
				$id = $this->request->getPost('peremajaan_id');
				$this->peremajaan->update($id, [
					'id_lokasi' => strip_tags($this->request->getPost('id_lokasi')),
					'tgl_req' => strip_tags($this->request->getPost('tgl_req')),
					'jenis_peremajaan' => strip_tags($this->request->getPost('jenis_peremajaan')),
					'status' => strip_tags($this->request->getPost('status')),
					'note' => strip_tags($this->request->getPost('note'))
				]);

				$result = [
					'success' => 'Data has been updated from database'
				];
			}
			echo json_encode($result);
		} else {
			exit('404 Not Found');
		}
	}

	public function deleteData()
	{
		if ($this->request->isAJAX()) {
			$id = $this->request->getVar('peremajaan_id');

			$this->peremajaan->delete($id);

			$result = [
				'output' => "Data has been deleted from database"
			];

			echo json_encode($result);
		} else {
			exit('404 Not Found');
		}
	}
}
