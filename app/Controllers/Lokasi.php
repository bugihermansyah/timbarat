<?php

namespace App\Controllers;

use App\Models\LokasiModel;
use App\Models\PerusahaanModel;
use App\Models\PicModel;
use App\Models\ProdukModel;

class Lokasi extends BaseController
{
    protected $lokasiModel;

    public function __construct()
    {
        $this->lokasiModel = new lokasiModel(); 
        $this->picModel = new picModel(); 
        $this->produkModel = new produkModel(); 
        $this->perusahaanModel = new perusahaanModel();   
        helper('form'); 
    }

    public function index()
    {
        $data = [
            'title' => 'Lokasi',
            'all_data' => $this->lokasiModel->select_data(), 
            'all_perusahaan' => $this->perusahaanModel->select_data(),
            'getPic' => $this->picModel->getPic(),
            'getProduk' => $this->produkModel->getProduk(),
        ];

        return view('lokasi/index', $data);
    }

    public function add_data()
    {
        // validation
        $rules = $this->validate([
            'lokasi' => 'required|is_unique[lokasi.lokasi]',
            'pic' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $data = [
            'id_perusahaan' => $this->request->getPost('id_perusahaan'),
            'lokasi' => $this->request->getPost('lokasi'),
            'id_produk' => $this->request->getPost('produk'),
            'pic' => $this->request->getPost('pic'),
            'contact' => $this->request->getPost('contact')
        ];

        $this->lokasiModel->add_data($data);
        session()->setFlashData('success', 'data has been added to database.');
        return redirect()->back();
    }

    public function delete_data($id)
    {
        $this->lokasiModel->delete_data($id);
        session()->setFlashData('success', 'data has been deleted from database.');
        return redirect()->back();
    }

    public function update_data($id)
    {
        // validation
        $rules = $this->validate([
            'lokasi' => 'required',
            'pic' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $data = [
            'lokasi' => $this->request->getPost('lokasi'),
            'pic' => $this->request->getPost('pic'),
            'contact' => $this->request->getPost('contact')
        ];

        $this->lokasiModel->update_data($id, $data);
        session()->setFlashData('success', 'data has been updated from database.');
        return redirect()->back();
    }
}
