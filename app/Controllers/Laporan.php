<?php

namespace App\Controllers;

use App\Models\LaporanModel;
use App\Models\LokasiModel;
use App\Models\RequestModel;
use App\Models\PicModel;
use App\Models\TagModel;
use App\Models\TagdetailModel;
use App\Models\LaporandetailModel;
use App\Models\RequestdetailModel;

class Laporan extends BaseController
{
    protected $laporanModel;
    protected $lokasiModel;
    protected $requestModel;
    protected $picModel;
    protected $tagModel;
    protected $tagdetailModel;
    protected $laporandetailModel;
    protected $requestdetailModel;

    public function __construct()
    {
        $this->laporanModel = new laporanModel(); 
        $this->lokasiModel = new lokasiModel(); 
        $this->requestModel = new requestModel(); 
        $this->picModel = new picModel(); 
        $this->tagModel = new tagModel(); 
        $this->tagdetailModel = new tagdetailModel(); 
        $this->laporandetailModel = new laporandetailModel();   
        $this->requestdetailModel = new requestdetailModel();   
    }

    public function index()
    {
        $data = [
            'title' => 'Laporan',
            'all_data' => $this->laporanModel->select_data(), // selecting all data
            'all_lokasi' => $this->lokasiModel->select_data(), // selecting all data lokasi
            'all_laporandetail' => $this->laporandetailModel->select_data(), // selecting all data lokasi
        ];

        return view('laporan/index', $data);
    }

    public function timeline($id)
    {
        $data = [
            'title' => 'Timeline Outstanding',
            'laporandetail' => $this->laporandetailModel->select_data($id),
            'laporanlokasi' => $this->laporanModel->select_data($id),
            'onsite' => $this->laporandetailModel->count_onsite($id),
            'getrequesttimeline' => $this->requestModel->getRequestTimeline($id),
            'remote' => $this->laporandetailModel->count_remote($id),
            'getTags' => $this->tagModel->getTags($id),
            'getLastData' => $this->laporandetailModel->getLastData($id)
        ];

        return view('laporan/view', $data);
    }

    public function add_data()
    {
        // validation
        $rules = $this->validate([
            'id_lokasi' => 'required',
            'pelapor' => 'required',
            'masalah' => 'required',
            'tgl_add' => 'required|date'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $cases = $this->request->getPost('masalah');
        for ($i=0; $i < count($cases); $i++) {
            $data[] = [
                'id_lokasi' => $this->request->getPost('id_lokasi'),
                'pelapor' => $this->request->getPost('pelapor'),
                'masalah' => $this->request->getPost('masalah')[$i],
                'tgl_add' => $this->request->getPost('tgl_add')
            ];
        }
        $this->laporanModel->add_data($data);
        session()->setFlashData('success', 'data has been added to database.');
        return redirect()->back();
    }

    function aksi($id)
    {
        $data = [
            'title' => 'Input detail laporan',
            'pics' => $this->picModel->getPic(),
            'getLaporan' => $this->laporanModel->select_data($id)
        ];
        return view('laporan/aksi', $data);
    }

    public function add_laporandetail()
    {
        // validation
        $rules = $this->validate([
            'id_laporan' => 'required',
            'sebab' => 'required',
            'solusi' => 'required',
            'status' => 'required',
            'datedetail' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $datalap = [
            'id_laporan' => $this->request->getPost('id_laporan'),
            'sebab' => $this->request->getPost('sebab'),
            'aksi' => $this->request->getPost('aksi'),
            'solusi' => $this->request->getPost('solusi'),
            'pic' => $this->request->getPost('pic'),
            'status' => $this->request->getPost('status'),
            'tglvisit' => $this->request->getPost('datedetail'),
            'onsite' => $this->request->getPost('onsite'),
            'note' => $this->request->getPost('note'),
            'revisit' => $this->request->getPost('revisit')
        ];
        $this->laporandetailModel->add_laporandetail($datalap);

        if(isset($_POST['item']) && !empty($_POST['item']))
        {
            $item = $this->request->getPost('item');
            $getIdLapDetail = $this->laporandetailModel->insertID();
            $datareq = [
                'id_lokasi' => $this->request->getPost('id_lokasi'),
                'id_laporan' => $this->request->getPost('id_laporan'),
                'id_laporandetail' => $getIdLapDetail,
                'tglrequest' => $this->request->getPost('datedetail')
            ];
            $this->requestModel->add_data($datareq);
            
            $getID = $this->requestModel->insertID();
            for ($i=0; $i < count($item); $i++) {
                $data[] = [
                    'id_request' => $getID,
                    'item' => $this->request->getPost('item')[$i]
                ];
            }
            $this->requestdetailModel->add_item($data);
        }
 
        session()->setFlashData('success', 'data has been added to database.');
        return redirect()->to('/laporan');
    }

    public function delete($id)
    {
        $this->laporanModel->delete_data($id);
        session()->setFlashData('success', 'data has been deleted from database.');
        return redirect()->back();
    }

    public function update_open($id)
    {
        $data = [
            'open' => $this->request->getPost('open')
        ];

        $this->laporanModel->update_data($id, $data);
        // session()->setFlashData('success', 'data has been updated from database.');
        // return redirect()->back();
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Edit Laporan',
            'all_tags' => $this->tagModel->getTags(),
            'getTagsId' => $this->tagModel->getTags($id),
            'getLaporan' => $this->laporanModel->select_data($id)
        ];

        return view('laporan/edit', $data);
    }

    public function edit_laporan()
    {
        $db = \Config\Database::connect();

        $laporan_id = $this->request->getPost('laporan_id');
        $tag_posts = $this->request->getPost('tags');

        $query   = $db->query('SELECT * FROM tags_detail WHERE id_laporan="$laporan_id"');
        $results = $query->getResult();
        
        $tag_array = [];
        foreach($results as $tag)
        {
            $tag_array[] = $tag->id_tag;
        }

        // insert newly add tag
        // $i = 0;
        foreach($tag_posts as $tag_post)
        {
            if (!in_array($tag_post, $tag_array)) 
            {
                $sqlinsert = "INSERT INTO tags_detail (id_tag, id_laporan) VALUES ('$tag_post','$laporan_id')";
            }
        }
        $db->query($sqlinsert);

        session()->setFlashData('success', 'data has been added to database.');
        return redirect()->to('/laporan');
    }    
}