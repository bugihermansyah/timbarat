<?php

namespace App\Controllers;

use App\Models\OutstandingModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Outstanding extends BaseController
{
    protected $outstandingModel;

    public function __construct()
    {
        $this->outstandingModel = new outstandingModel();   
        helper('form'); 
    }

    public function index()
    {
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $label = 'Data laporan Tanggal '.date('d-m-y', strtotime($tgl));
                $url_export = 'outstanding/export?filter=1&tanggal='.$tgl;
                $laporan = $this->outstandingModel->getByDate($tgl); // Panggil fungsi view_by_date yang ada di laporanModel
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $label = 'Data laporan Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $url_export = 'outstanding/export?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $laporan = $this->outstandingModel->getByMonth($bulan, $tahun); // Panggil fungsi view_by_month yang ada di laporanModel
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $label = 'Data laporan Tahun '.$tahun;
                $url_export = 'outstanding/export?filter=3&tahun='.$tahun;
                $laporan = $this->outstandingModel->getByYear($tahun); // Panggil fungsi view_by_year yang ada di laporanModel
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $label = 'Semua Data laporan';
            $url_export = 'laporan/export';
            $laporan = $this->outstandingModel->view_all(); // Panggil fungsi view_all yang ada di laporanModel
        }

        $data['title'] = "Export : Outstanding";
		$data['label'] = $label;
		$data['url_export'] = base_url('/'.$url_export);
		$data['laporan'] = $laporan;
        $data['option_tahun'] = $this->outstandingModel->getYear();
		return view('report/outstanding', $data);
    }

    public function exportOutstanding(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $label = 'Data laporan Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->OutstandingModel->getByDate($tgl); // Panggil fungsi view_by_date yang ada di laporanModel
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $label = 'Data laporan Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->OutstandingModel->getByMonth($bulan, $tahun); // Panggil fungsi view_by_month yang ada di laporanModel
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $label = 'Data laporan Tahun '.$tahun;
                $laporan = $this->outstandingModel->getByYear($tahun); // Panggil fungsi view_by_year yang ada di TransaksiModel
            }
        }

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(17);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(80);
        
        // tulis header/nama kolom 
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'Lokasi')
            ->setCellValue('C1', 'Masalah')
            ->setCellValue('D1', 'Sebab')
            ->setCellValue('E1', 'Solusi');

        $column = 2;
        // tulis data outstanding ke cell
        $no = 1;
        foreach ($laporan as $data) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $column, $no++)
                ->setCellValue('B' . $column, $data->name)
                ->setCellValue('C' . $column, $data->masalah)
                ->setCellValue('D' . $column, $data->sebab)
                ->setCellValue('E' . $column, $data->solusi);
            $column++;
        }

        // tulis dalam format .xlsx
        $writer = new Xlsx($spreadsheet);
        $fileName = 'Data Laporan Oustanding';

        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $fileName . '.xlsx');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}
