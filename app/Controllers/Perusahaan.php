<?php

namespace App\Controllers;

use App\Models\PerusahaanModel;

class Perusahaan extends BaseController
{
    protected $perusahaanModel;

    public function __construct()
    {
        $this->perusahaanModel = new perusahaanModel();   
        helper('form'); 
    }

    public function index()
    {
        $data = [
            'title' => 'Perusahaan',
            'all_data' => $this->perusahaanModel->select_data() // selecting all data
        ];

        return view('perusahaan/index', $data);
    }

    public function add_data()
    {
        // validation
        $rules = $this->validate([
            'name' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $data = [
            'name' => $this->request->getPost('name')
        ];

        $this->perusahaanModel->add_data($data);
        session()->setFlashData('success', 'data has been added to database.');
        return redirect()->back();
    }

    public function delete_data($id)
    {
        $this->perusahaanModel->delete_data($id);
        session()->setFlashData('success', 'data has been deleted from database.');
        return redirect()->back();
    }

    public function update_data($id)
    {
        // validation
        $rules = $this->validate([
            'name' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $data = [
            'name' => $this->request->getPost('name')
        ];

        $this->perusahaanModel->update_data($id, $data);
        session()->setFlashData('success', 'data has been updated from database.');
        return redirect()->back();
    }
}
