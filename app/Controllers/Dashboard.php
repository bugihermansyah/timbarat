<?php

namespace App\Controllers;

use App\Models\LaporanModel;
use App\Models\RequestModel;
use App\Models\LokasiModel;

class Dashboard extends BaseController
{
    protected $laporanModel;
    protected $requestModel;
    protected $lokasiModel;

    public function __construct()
    {
        $this->laporanModel = new laporanModel();
        $this->requestModel = new requestModel();
        $this->lokasiModel = new lokasiModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Reminder Tim Barat',
            // bugi
            'count_open_bugi' => $this->laporanModel->select_count_open_bugi(),
            'count_req_bugi' => $this->requestModel->select_count_req_bugi(),
            // roma
            'count_open_roma' => $this->laporanModel->select_count_open_roma(),
            'count_req_roma' => $this->requestModel->select_count_req_roma(),
            // fajar
            'count_open_fajar' => $this->laporanModel->select_count_open_fajar(),
            'count_req_fajar' => $this->requestModel->select_count_req_fajar(),
            // hasbu
            'count_open_hasbu' => $this->laporanModel->select_count_open_hasbu(),
            'count_req_hasbu' => $this->requestModel->select_count_req_hasbu(),
            // husein
            'count_open_husein' => $this->laporanModel->select_count_open_husein(),
            'count_req_husein' => $this->requestModel->select_count_req_husein(),
            // dhaysal
            'count_open_dhaysal' => $this->laporanModel->select_count_open_dhaysal(),
            'count_req_dhaysal' => $this->requestModel->select_count_req_dhaysal(),
            // dana
            'count_open_dana' => $this->laporanModel->select_count_open_dana(),
            'count_req_dana' => $this->requestModel->select_count_req_dana(),
            // all
            'count_lokasi_all' => $this->lokasiModel->select_count_lokasi_all(),
            'count_open_all' => $this->laporanModel->select_count_open_all(),
            'count_req_all' => $this->requestModel->select_count_req_all(),
            'all_jatuhtempo' => $this->requestModel->select_jatuhtempo(),
            'select_laporan' => $this->laporanModel->select_laporan()
        ];

        return view('dashboard/index', $data);
    }
}