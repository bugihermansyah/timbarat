<?php

namespace App\Controllers;

use App\Models\LokasiModel;
use App\Models\RequestModel;
use App\Models\LaporanModel;
use App\Models\RequestdetailModel;

class Request extends BaseController
{
    protected $lokasiModel;
    protected $requestModel;
    protected $laporanModel;
    protected $requestdetailModel;

    public function __construct()
    {
        $this->lokasiModel = new lokasiModel(); 
        $this->requestModel = new requestModel();  
        $this->laporanModel = new laporanModel(); 
        $this->requestdetailModel = new requestdetailModel();   
        helper('form'); 
    }

    public function index()
    {
        $data = [
            'title' => 'Request',
            'all_lokasi' => $this->lokasiModel->select_lokasi(),
            'all_laporan' => $this->laporanModel->getLaporanDetailByOpen(),
            'all_data' => $this->requestModel->select_data(),
            'all_unit' => $this->requestdetailModel->select_unit() // selecting all data
        ];

        return view('request/index', $data);
    }

    public function add_item()
    {
        // validation
        // $rules = $this->validate([
        //     'item[]' => 'required'
        // ]);

        // if (!$rules) {
        //     session()->setFlashData('failed', \Config\Services::validation()->getErrors());
        //     return redirect()->back();
        // }
        $datareq = [
            'id_lokasi' => $this->request->getPost('id_lokasi'),
            'id_laporandetail' => $this->request->getPost('id_laporandetail'),
            'tglrequest' => $this->request->getPost('tglrequest'),
            'status' => 'Open'
        ];

        $this->requestModel->add_data($datareq);
        $getID = $this->requestModel->insertID();

        $item = $this->request->getPost('item');
        for ($i=0; $i < count($item); $i++) {
            $data[] = [
                'id_request' => $getID,
                'item' => $this->request->getPost('item')[$i]
            ];
        }

        $this->requestdetailModel->add_item($data);
        session()->setFlashData('success', 'data has been added to database.');
        return redirect()->back();
    }

    public function add_data()
    {
        //validation
        $rules = $this->validate([
            'id_lokasi' => 'required',
            'tglrequest' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }
        
        $data = [
            'id_lokasi' => $this->request->getPost('id_lokasi'),
            'tglrequest' => $this->request->getPost('tglrequest'),
            'status' => 'Open'
        ];

        $this->requestModel->add_data($data);
        session()->setFlashData('success', 'data has been added to database.');
        return redirect()->back();
    }

    public function delete_data($id)
    {
        $this->requestdetailModel->delete_data($id);
        $this->requestModel->delete_data($id);
        session()->setFlashData('success', 'data has been deleted from database.');
        return redirect()->back();
    }

    public function update_data($id)
    {
        // validation
        $rules = $this->validate([
            'name' => 'required',
            'pic' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $data = [
            'name' => $this->request->getPost('name'),
            'pic' => $this->request->getPost('pic')
        ];

        $this->requestModel->update_data($id, $data);
        session()->setFlashData('success', 'data has been updated from database.');
        return redirect()->back();
    }

    public function update_pengiriman($id)
    {
        // validation
        $rules = $this->validate([
            'tglpengiriman' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }

        $data = [
            'tglpengiriman' => $this->request->getPost('tglpengiriman')
        ];

        $this->requestModel->update_pengiriman($id, $data);
        session()->setFlashData('success', 'data has been updated from database.');
        return redirect()->back();
    }

    public function update_pengambilan($id)
    {
        // validation
        $rules = $this->validate([
            'tglpengambilan' => 'required'
        ]);

        if (!$rules) {
            session()->setFlashData('failed', \Config\Services::validation()->getErrors());
            return redirect()->back();
        }
        
        $ids = $this->request->getPost('id');
        for ($a=0; $a < count($ids); $a++) {
            $data[] = [
                'requestdetail_id' => $this->request->getPost('id')[$a],
                'status' => $this->request->getPost('status')[$a]
            ];
            $this->requestdetailModel->update_status($data, $id);
        }

        $data = [
            'tglpengambilan' => $this->request->getPost('tglpengambilan')
        ];
        $this->requestModel->update_pengambilan($id, $data);

        session()->setFlashData('success', 'data has been updated from database.');
        return redirect()->back();
    }

    public function update_status($id)
    {
        $data = [
            'status' => $this->request->getPost('status')
        ];

        $this->requestModel->update_data($id, $data);
        session()->setFlashData('success', 'data has been updated from database.');
        return redirect()->back();
    }
}
